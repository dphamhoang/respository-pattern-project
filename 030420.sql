USE [Web4gBussiness]
GO
/****** Object:  Table [dbo].[Albums]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Albums](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[ResourcePath] [nvarchar](500) NOT NULL,
	[ImageFolder] [nvarchar](500) NULL,
	[IsVideo] [bit] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[Order] [datetime] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK_Albums] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Articles]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Articles](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
	[SeName] [nvarchar](500) NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[ViewCount] [int] NOT NULL,
	[LastUpdatedBy] [nvarchar](200) NULL,
	[IsPublished] [bit] NOT NULL,
	[Category_Id] [bigint] NOT NULL,
	[Language_Id] [int] NOT NULL,
	[IsHot] [bit] NOT NULL,
	[ImageFolder] [nvarchar](500) NULL,
	[Order] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[MetaTitle] [nvarchar](500) NULL,
	[MetaImage] [nvarchar](2000) NULL,
	[MetaKeywords] [nvarchar](500) NULL,
	[MetaDescription] [nvarchar](500) NULL,
	[ExternalUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK__Articles__3214EC0703317E3D] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Banners]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Banners](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[IsPublished] [bit] NOT NULL,
	[Language_Id] [int] NOT NULL,
	[Link] [nvarchar](500) NULL,
	[Alt] [nvarchar](500) NULL,
	[Path] [nvarchar](500) NULL,
	[Order] [int] NOT NULL,
	[Parent_Id] [bigint] NULL,
	[ImageFolder] [nvarchar](500) NULL,
	[IsVideo] [bit] NOT NULL,
 CONSTRAINT [PK_SlideShowAdvertise] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Branches]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Branches](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Language_Id] [int] NOT NULL,
	[Region_Id] [int] NULL,
	[OfficeName] [nvarchar](200) NOT NULL,
	[Address] [nvarchar](200) NULL,
	[AddressMap] [nvarchar](200) NULL,
	[Lng] [decimal](18, 7) NULL,
	[Lat] [decimal](18, 7) NULL,
	[Phone] [nvarchar](50) NULL,
	[Email] [nvarchar](200) NULL,
	[Description] [nvarchar](200) NULL,
	[Fax] [nvarchar](100) NULL,
	[IsPublished] [bit] NOT NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK_Branch] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](255) NULL,
	[SeName] [nvarchar](500) NULL,
	[Parent_Id] [bigint] NULL,
	[Language_Id] [int] NOT NULL,
	[IsSystem] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[DisplayTemplate] [int] NOT NULL,
	[EntityType] [int] NOT NULL,
	[PrivateArea] [bit] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](500) NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[MetaTitle] [nvarchar](500) NULL,
	[MetaImage] [nvarchar](2000) NULL,
	[MetaKeywords] [nvarchar](500) NULL,
	[MetaDescription] [nvarchar](500) NULL,
	[ExternalUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK__Categori__3214EC070CBAE877] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contacts]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacts](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](200) NOT NULL,
	[Address] [nvarchar](200) NOT NULL,
	[Content] [nvarchar](4000) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK__Contacts__3214EC07DB4D0973] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FaqItems]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FaqItems](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Order] [int] NOT NULL,
	[AskerName] [nvarchar](max) NULL,
	[AskerPhone] [nvarchar](max) NULL,
	[AskerEmail] [nvarchar](max) NULL,
	[AskerAddress] [nvarchar](max) NULL,
	[AskTime] [datetime] NOT NULL,
	[AskTitle] [nvarchar](max) NULL,
	[AskContent] [nvarchar](max) NULL,
	[AnswerName] [nvarchar](max) NULL,
	[AnswerTime] [datetime] NOT NULL,
	[AnswerContent] [nvarchar](max) NULL,
	[ApprovalStatus] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Images]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Images](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Order] [int] NOT NULL,
	[AltImage] [nvarchar](200) NULL,
	[ImageDefault] [nvarchar](max) NOT NULL,
	[Video] [nvarchar](50) NULL,
	[Article_Id] [bigint] NOT NULL,
 CONSTRAINT [PK_Images] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Introductions]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Introductions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](1000) NULL,
	[Content] [nvarchar](max) NULL,
	[Image] [nvarchar](4000) NULL,
	[ImageFolder] [nvarchar](1000) NULL,
	[Order] [tinyint] NOT NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK_Introductions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Languages]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Languages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Key] [nvarchar](10) NOT NULL,
	[Image] [nvarchar](200) NULL,
	[IsDefault] [bit] NOT NULL,
 CONSTRAINT [PK__Language__3214EC0732E0915F] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LogHistories]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogHistories](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EntityType] [int] NOT NULL,
	[EntityId] [bigint] NOT NULL,
	[ActionType] [int] NOT NULL,
	[ActionBy] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Comment] [nvarchar](200) NULL,
 CONSTRAINT [PK__LogHisto__3214EC073A81B327] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Medias]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Medias](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Order] [int] NOT NULL,
	[Alt] [nvarchar](200) NULL,
	[Path] [nvarchar](max) NOT NULL,
	[Album_Id] [bigint] NOT NULL,
 CONSTRAINT [PK_Medias] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MenuItems]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItems](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Order] [int] NOT NULL,
	[Link] [nvarchar](500) NULL,
	[LinkTarget] [int] NOT NULL,
	[Parent_Id] [bigint] NULL,
	[Language_Id] [int] NOT NULL,
	[Menu_Id] [bigint] NOT NULL,
	[Image] [nvarchar](200) NULL,
 CONSTRAINT [PK__MenuItem__3214EC0745F365D3] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Menus]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menus](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CodeName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[Type] [int] NOT NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK__Menus__3214EC0749C3F6B7] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Regions]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Regions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Lng] [float] NOT NULL,
	[Lat] [float] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[RegionType] [int] NOT NULL,
	[Parent_Id] [int] NULL,
	[ZipCode] [nvarchar](200) NULL,
	[FeeShip] [decimal](18, 2) NULL,
 CONSTRAINT [PK__Regions__3214EC075535A963] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RolePrivilleges]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolePrivilleges](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PermissionType] [int] NOT NULL,
	[FeatureType] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_RolePrivilleges] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Settings]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](50) NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK__Settings__3214EC075AEE82B9] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StringResourceKeys]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StringResourceKeys](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK__StringRe__3214EC075EBF139D] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StringResourceValues]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StringResourceValues](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[Key_Id] [bigint] NULL,
	[Language_Id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Subscribes]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subscribes](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](200) NOT NULL,
	[Note] [nvarchar](500) NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Subscribes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UrlRecords]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UrlRecords](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SeName] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Url] [nvarchar](max) NULL,
	[OriginUrl] [nvarchar](max) NULL,
	[EntityId] [bigint] NOT NULL,
	[EntityType] [int] NOT NULL,
	[Language_Id] [int] NULL,
	[IsDisabled] [bit] NULL,
 CONSTRAINT [PK__UrlRecor__3214EC0768487DD7] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProfile](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[FullName] [nvarchar](400) NULL,
	[Phone] [nvarchar](20) NULL,
	[Gender] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[LastedIP] [nvarchar](max) NULL,
	[GeneratePassword] [nvarchar](max) NULL,
	[BirthDay] [datetime] NOT NULL,
	[Image] [nvarchar](500) NULL,
	[Provider] [tinyint] NOT NULL,
 CONSTRAINT [PK__UserProf__1788CC4CBB3D02C2] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_Membership]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Membership](
	[UserId] [int] NOT NULL,
	[CreateDate] [datetime] NULL,
	[ConfirmationToken] [nvarchar](max) NULL,
	[IsConfirmed] [bit] NULL,
	[LastPasswordFailureDate] [datetime] NULL,
	[PasswordFailuresSinceLastSuccess] [int] NOT NULL,
	[Password] [nvarchar](max) NULL,
	[PasswordChangedDate] [datetime] NOT NULL,
	[PasswordSalt] [nvarchar](max) NULL,
	[PasswordVerificationToken] [nvarchar](max) NULL,
	[PasswordVerificationTokenExpirationDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_OAuthMembership]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_OAuthMembership](
	[Provider] [nvarchar](128) NOT NULL,
	[ProviderUserId] [nvarchar](128) NOT NULL,
	[UserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Provider] ASC,
	[ProviderUserId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_Roles]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](max) NULL,
	[RoleDescription] [nvarchar](max) NULL,
	[IsSystem] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_UsersInRoles]    Script Date: 04/03/2020 13:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_UsersInRoles](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([Id], [Title], [SeName], [Parent_Id], [Language_Id], [IsSystem], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Description], [ImageDefault], [ImageFolder], [CreateDate], [UpdateDate], [MetaTitle], [MetaImage], [MetaKeywords], [MetaDescription], [ExternalUrl]) VALUES (1, NULL, N'root-news-category', NULL, 1, 1, 0, 0, 3, 0, NULL, NULL, NULL, CAST(N'2018-12-25T00:00:00.000' AS DateTime), CAST(N'2018-12-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [SeName], [Parent_Id], [Language_Id], [IsSystem], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Description], [ImageDefault], [ImageFolder], [CreateDate], [UpdateDate], [MetaTitle], [MetaImage], [MetaKeywords], [MetaDescription], [ExternalUrl]) VALUES (2, N'Trang chủ', N'trang-chu', 1, 1, 0, 0, 1, 1, 0, NULL, NULL, N'/Upload/Category/4e8ca998-db9d-4e5d-b6c8-f99f1cd5724a', CAST(N'2020-01-15T15:54:39.590' AS DateTime), CAST(N'2020-01-15T15:54:39.590' AS DateTime), NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[Languages] ON 

INSERT [dbo].[Languages] ([Id], [Name], [Key], [Image], [IsDefault]) VALUES (1, N'Vietnamese', N'vi', N'/Content/admin/assets/images/nation/1.png', 1)
INSERT [dbo].[Languages] ([Id], [Name], [Key], [Image], [IsDefault]) VALUES (2, N'English', N'en', N'/Content/admin/assets/images/nation/2.png', 0)
SET IDENTITY_INSERT [dbo].[Languages] OFF
SET IDENTITY_INSERT [dbo].[LogHistories] ON 

INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (2, 1, 2, 2, N'administrator', CAST(N'2020-01-15T15:54:23.870' AS DateTime), N'Đã tạo mới Danh mục')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (3, 1, 0, 1, N'administrator', CAST(N'2020-01-15T15:55:43.500' AS DateTime), N'Khởi tạo Danh mục')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (4, 1, 3, 2, N'administrator', CAST(N'2020-01-15T15:56:23.787' AS DateTime), N'Đã tạo mới Danh mục')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (5, 1, 3, 4, N'administrator', CAST(N'2020-01-15T15:57:26.913' AS DateTime), N'Đã xóa Danh mục')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10002, 1, 0, 1, N'administrator', CAST(N'2020-02-14T15:27:55.317' AS DateTime), N'Khởi tạo Danh mục')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10003, 0, 2596, 2, N'administrator', CAST(N'2020-02-20T14:08:04.633' AS DateTime), N'User login at ::1 () thiết bị Web Browser')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10004, 17, 2, 4, N'administrator', CAST(N'2020-02-20T14:26:08.043' AS DateTime), N'Đã xóa Banner')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10005, 1, 0, 1, N'administrator', CAST(N'2020-02-20T14:26:15.643' AS DateTime), N'Khởi tạo Danh mục')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10006, 1, 0, 1, N'administrator', CAST(N'2020-02-20T14:26:49.953' AS DateTime), N'Khởi tạo Danh mục')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10007, 2, 0, 1, N'administrator', CAST(N'2020-02-20T14:35:08.773' AS DateTime), N'Khởi tạo Bài viết')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10008, 5, 0, 1, N'administrator', CAST(N'2020-02-20T14:36:20.340' AS DateTime), N'Khởi tạo Thư viện')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10009, 5, 0, 1, N'administrator', CAST(N'2020-02-20T14:38:36.613' AS DateTime), N'Khởi tạo Thư viện')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10010, 5, 0, 1, N'administrator', CAST(N'2020-02-20T14:39:21.403' AS DateTime), N'Khởi tạo Thư viện')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10011, 5, 0, 1, N'administrator', CAST(N'2020-02-20T14:39:51.130' AS DateTime), N'Khởi tạo Thư viện')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10012, 2, 0, 1, N'administrator', CAST(N'2020-02-20T14:40:38.970' AS DateTime), N'Khởi tạo Bài viết')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10013, 5, 0, 1, N'administrator', CAST(N'2020-02-20T14:40:47.547' AS DateTime), N'Khởi tạo Thư viện')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10014, 5, 0, 1, N'administrator', CAST(N'2020-02-20T14:41:14.773' AS DateTime), N'Khởi tạo Thư viện')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10015, 5, 0, 1, N'administrator', CAST(N'2020-02-20T14:41:30.157' AS DateTime), N'Khởi tạo Thư viện')
SET IDENTITY_INSERT [dbo].[LogHistories] OFF
SET IDENTITY_INSERT [dbo].[Menus] ON 

INSERT [dbo].[Menus] ([Id], [CodeName], [Description], [Type], [Order]) VALUES (1, N'main-menu', N'Quản lý các Menu ở đầu trang', 0, 1)
INSERT [dbo].[Menus] ([Id], [CodeName], [Description], [Type], [Order]) VALUES (2, N'right-menu', N'Quản lý các Menu ở bên phải của trang', 3, 2)
INSERT [dbo].[Menus] ([Id], [CodeName], [Description], [Type], [Order]) VALUES (3, N'footer-menu', N'Quản lý các Menu ở chân trang', 1, 3)
SET IDENTITY_INSERT [dbo].[Menus] OFF
SET IDENTITY_INSERT [dbo].[Settings] ON 

INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (1, N'SmtpServer', N'smtp.gmail.com')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (2, N'SmtpPort', N'587')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (3, N'UserEmailAccountName', N'ContactOctopuze@gmail.com')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (4, N'EmailAccountPassword', N'0905010115')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (6, N'EnableSsl', N'true')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (7, N'FacebookPage', N'https://www.facebook.com')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (8, N'YoutubePage', N'https://www.Youtube.com')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (9, N'TwitterPage', N'https://twitter.com/')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (10, N'EmailCC', N'khoa.nguyen@web4gsolutions.com')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (12, N'SiteKeyRecaptcha', N'6Ldl_HgUAAAAAEavOV7_7CsUQ6LyirctL9dWjfCK')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (13, N'SecretKeyRecaptcha', N'6Ldl_HgUAAAAALXEM2yEw34jbY0VnNLrViqGFPff')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (14, N'KeyAPIGoogleMap', N'AIzaSyD446K2c9ddRvzCq33chkUAyZnjibJ1R3I')
SET IDENTITY_INSERT [dbo].[Settings] OFF
SET IDENTITY_INSERT [dbo].[StringResourceKeys] ON 

INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (2, N'Contact_success')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (3, N'Error_Title')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (4, N'Error_Content')
SET IDENTITY_INSERT [dbo].[StringResourceKeys] OFF
SET IDENTITY_INSERT [dbo].[StringResourceValues] ON 

INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (2, N'Contact_success', 2, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (3, N'Contact_success', 2, 2)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (4, N'Error_Title', 3, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (5, N'Error_Title', 3, 2)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (6, N'Error_Content', 4, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (7, N'Error_Content', 4, 2)
SET IDENTITY_INSERT [dbo].[StringResourceValues] OFF
SET IDENTITY_INSERT [dbo].[UrlRecords] ON 

INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [OriginUrl], [EntityId], [EntityType], [Language_Id], [IsDisabled]) VALUES (1, N'trang-chu', N'Trang chủ', N'/trang-chu', N'/trang-chu', 2, 1, 1, 0)
SET IDENTITY_INSERT [dbo].[UrlRecords] OFF
SET IDENTITY_INSERT [dbo].[UserProfile] ON 

INSERT [dbo].[UserProfile] ([UserId], [UserName], [Email], [FullName], [Phone], [Gender], [IsActive], [LastedIP], [GeneratePassword], [BirthDay], [Image], [Provider]) VALUES (2596, N'Administrator', N'abc@abc.com', NULL, NULL, 0, 1, NULL, NULL, CAST(N'2019-03-04T08:26:38.047' AS DateTime), NULL, 0)
SET IDENTITY_INSERT [dbo].[UserProfile] OFF
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (2596, CAST(N'2019-03-04T08:26:38.047' AS DateTime), NULL, 1, CAST(N'2019-12-21T10:28:32.360' AS DateTime), 0, N'AOZ63vW4OO0WLJN7xY2pbII48L0yrdGncUczzhUspvSGUPDnTggQRxAKQH2UFG9L1w==', CAST(N'2019-03-04T08:26:38.047' AS DateTime), N'', NULL, NULL)
SET IDENTITY_INSERT [dbo].[webpages_Roles] ON 

INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName], [RoleDescription], [IsSystem]) VALUES (1, N'Admin', N'Admin', 0)
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName], [RoleDescription], [IsSystem]) VALUES (2, N'Mod', NULL, 0)
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName], [RoleDescription], [IsSystem]) VALUES (5, N'Guest', NULL, 0)
SET IDENTITY_INSERT [dbo].[webpages_Roles] OFF
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (2596, 1)
ALTER TABLE [dbo].[Banners] ADD  CONSTRAINT [DF_IsPublished]  DEFAULT ('false') FOR [IsPublished]
GO
ALTER TABLE [dbo].[Banners] ADD  CONSTRAINT [DF__Banners__IsVideo__01142BA1]  DEFAULT ((0)) FOR [IsVideo]
GO
ALTER TABLE [dbo].[Regions] ADD  CONSTRAINT [DF_Regions_FeeShip]  DEFAULT ((0)) FOR [FeeShip]
GO
ALTER TABLE [dbo].[UrlRecords] ADD  CONSTRAINT [DF_UrlRecords_IsDisabled]  DEFAULT ((0)) FOR [IsDisabled]
GO
ALTER TABLE [dbo].[webpages_Roles] ADD  CONSTRAINT [DF_webpages_Roles_IsSystem]  DEFAULT ((0)) FOR [IsSystem]
GO
ALTER TABLE [dbo].[Albums]  WITH CHECK ADD  CONSTRAINT [FK_Albums_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[Albums] CHECK CONSTRAINT [FK_Albums_Languages]
GO
ALTER TABLE [dbo].[Articles]  WITH CHECK ADD  CONSTRAINT [Article_Category] FOREIGN KEY([Category_Id])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Articles] CHECK CONSTRAINT [Article_Category]
GO
ALTER TABLE [dbo].[Articles]  WITH CHECK ADD  CONSTRAINT [Article_Language] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[Articles] CHECK CONSTRAINT [Article_Language]
GO
ALTER TABLE [dbo].[Branches]  WITH CHECK ADD  CONSTRAINT [FK_Branch_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[Branches] CHECK CONSTRAINT [FK_Branch_Languages]
GO
ALTER TABLE [dbo].[Branches]  WITH CHECK ADD  CONSTRAINT [FK_Branches_Regions] FOREIGN KEY([Region_Id])
REFERENCES [dbo].[Regions] ([Id])
GO
ALTER TABLE [dbo].[Branches] CHECK CONSTRAINT [FK_Branches_Regions]
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD  CONSTRAINT [Category_Parent] FOREIGN KEY([Parent_Id])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Categories] CHECK CONSTRAINT [Category_Parent]
GO
ALTER TABLE [dbo].[Images]  WITH CHECK ADD  CONSTRAINT [FK_Images_Articles] FOREIGN KEY([Article_Id])
REFERENCES [dbo].[Articles] ([Id])
GO
ALTER TABLE [dbo].[Images] CHECK CONSTRAINT [FK_Images_Articles]
GO
ALTER TABLE [dbo].[Introductions]  WITH CHECK ADD  CONSTRAINT [FK_Introductions_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[Introductions] CHECK CONSTRAINT [FK_Introductions_Languages]
GO
ALTER TABLE [dbo].[Medias]  WITH CHECK ADD  CONSTRAINT [FK_Medias_Albums] FOREIGN KEY([Album_Id])
REFERENCES [dbo].[Albums] ([Id])
GO
ALTER TABLE [dbo].[Medias] CHECK CONSTRAINT [FK_Medias_Albums]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [FK_MenuItems_Menus] FOREIGN KEY([Menu_Id])
REFERENCES [dbo].[Menus] ([Id])
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [FK_MenuItems_Menus]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [MenuItem_Language] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [MenuItem_Language]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [MenuItem_Parent] FOREIGN KEY([Parent_Id])
REFERENCES [dbo].[MenuItems] ([Id])
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [MenuItem_Parent]
GO
ALTER TABLE [dbo].[Regions]  WITH CHECK ADD  CONSTRAINT [Region_Parent] FOREIGN KEY([Parent_Id])
REFERENCES [dbo].[Regions] ([Id])
GO
ALTER TABLE [dbo].[Regions] CHECK CONSTRAINT [Region_Parent]
GO
ALTER TABLE [dbo].[StringResourceValues]  WITH CHECK ADD  CONSTRAINT [StringResourceValue_Key] FOREIGN KEY([Key_Id])
REFERENCES [dbo].[StringResourceKeys] ([Id])
GO
ALTER TABLE [dbo].[StringResourceValues] CHECK CONSTRAINT [StringResourceValue_Key]
GO
ALTER TABLE [dbo].[StringResourceValues]  WITH CHECK ADD  CONSTRAINT [StringResourceValue_Language] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[StringResourceValues] CHECK CONSTRAINT [StringResourceValue_Language]
GO
ALTER TABLE [dbo].[UrlRecords]  WITH CHECK ADD  CONSTRAINT [UrlRecord_Language] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[UrlRecords] CHECK CONSTRAINT [UrlRecord_Language]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [UserInRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[webpages_Roles] ([RoleId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [UserInRole_Role]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [UserInRole_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [UserInRole_User]
GO
