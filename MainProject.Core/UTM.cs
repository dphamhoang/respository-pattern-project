﻿namespace MainProject.Core
{
    public class UTM
    {
        public string IP { get; set; }

        public string Source { get; set; }

        public string Medium { get; set; }

        public string Campaign { get; set; }

        public string Term { get; set; }

        public string UTMContent { get; set; }

        public string Social { get; set; }

        public string SocialId { get; set; }
    }
}
