﻿namespace MainProject.Core
{
    public class Seo
    {
        public string MetaTitle { get; set; }

        public string MetaDescription { get; set; }

        public string MetaKeywords { get; set; }

        public string MetaImage { get; set; }

        public string ExternalUrl { get; set; }
    }
}
