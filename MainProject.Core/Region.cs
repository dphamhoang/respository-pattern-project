﻿using System.ComponentModel.DataAnnotations;
using MainProject.Core.Enums;

namespace MainProject.Core
{
    public class Region
    {
        [Key]
        public int Id { get; set; }

        public double Lng { get; set; }

        public double Lat { get; set; }

        public string Name { get; set; }

        public string ZipCode { get; set; }

        public decimal? FeeShip { get; set; }

        public virtual Region Parent { get; set; }

        public RegionTypeCollection RegionType { get; set; }
    }
}
