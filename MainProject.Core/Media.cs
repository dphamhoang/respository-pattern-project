﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MainProject.Core
{
	[Table("Medias")]
    public class Media : Image
    {
        public virtual Album Album { get; set; }
    }
}
