﻿namespace MainProject.Core.Enums
{
    public enum ProviderTypeCollection : byte
    {
        None,

        Google,

        Facebook,
    }
}
