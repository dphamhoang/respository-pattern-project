﻿using System.ComponentModel;

namespace MainProject.Core.Enums
{
    public enum EntityTypeCollection
    {
        [Description("Đăng nhập")]
        Login = 0,

        [Description("Danh mục")]
        Categories = 1,

        [Description("Bài viết")]
        Articles = 2,

        Images = 3,

        Email = 4,

        [Description("Thư viện")]
        Gallery = 5,

        [Description("Liên hệ")]
        Contact = 6,

        [Description("Chi nhánh")]
        Branch = 7,

        [Description("Tài khoản quản trị")]
        User = 8,

        [Description("Mục lục")]
        Menu = 9,

        [Description("Hỏi đáp")]
        FaqItem = 10,

        [Description("Phân quyền")]
        Permission = 11,

        [Description("Vùng miền")]
        Region = 12,

        [Description("Thiết lập")]
        Setting = 14,

        [Description("Tài nguyên")]
        Resource = 15,

        [Description("Đăng ký Email")]
        Subscribe = 16,

        Banner = 17,

        Section = 18,

        Brand = 20,

        CommerceCategory = 21,

        CommerceProduct = 22,
    }
}
