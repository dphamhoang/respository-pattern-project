﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainProject.Core
{
    public class Image
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public int Order { get; set; }

        public string Alt { get; set; }

        public string Path { get; set; }
    }
}
