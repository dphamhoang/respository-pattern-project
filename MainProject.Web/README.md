- Configuration steps:
1. Open project by file has extension is '.sln'.
2. Use Ctrl + F5 to run source.
2.1. If project can't be run, please expand Reference and check it is invalid library in Solution Explorer.
2.2. To fix invalid, enter this command "Update-Package -reinstall" to Tools -> Nuget Package Manager -> Package Manager Console.
3. Before commit source please turn off visual studio and choose "save" if any question's appeared, just ignore folder bin, obj, .vs, packages.
4. Content files just upload into App_Data and gitnore this folder.

- Landing Page Rule coding:
1. Put the frontend css, js,... folders and files in /Content/resources

2. Define css + js tags in _Layout.cshtml following the instruction.

3. Get variables in model by using: @Model.[Name].[Name]  
    *Note: Some variable can be null so you should use condition before using it.

4. Get variables in list item using: 
    @foreach(var item in Model.[Name]) { 
        // Put html items here 
    }

5. Get resource data with language steps:
    - Named Key:
        + Common: is the key often used. Ex: Search, Read_More, Detail,...
        + Specified Location: is the key exist at some specified place. Ex: Contact_Address, News_Others,...
        + Validation Key: Ask Backend developers to get more information.
    - Using this code in View: @Html.GetResource(TempResourceKeyCollection.[Name])

6. Split a "Form" into other View
        - Replace a Form tag "<form>" by 
            @using(Html.BeginForm()) { 
                // Define input's here
                @Html.TextBoxFor(d => d.[Name]) for input has type is text
                @Html.EditorFor(d => d.[Name]) for input has type is number
                @Html.TextAreaFor(d => d.[Name]) for textarea
            }   

7. Get a "Form" from other View
    - Using: @Html.Partial("[Name]", new [Model]()) 
            Ex: @Html.Partial("_Subscribe", new MainProject.Bussiness.Models.SubscribeModel())

8. Render data from Model property to html: @Html.Raw(Model.[Name])

9. Get list items in a specified Section section:
    - Model: create static data by using:
            Image = JsonHelper.Serialize(new List<Image> {
                new Image {
                    // Define Property is here
                },
                ...
            })
    - View: 
            + Define library at the head of this View: @using MainProject.Core
            + Get list items in single property by using:
                @foreach (var item in JsonHelper.Deserialize<List<Image>>(Model.Partner.Image) ?? new List<Image>())
                {
                    // Do something
                }

10. Create url redirect for other template page:
    - Create Controller, Define Action in Controller in to Controllers folder
    - Create Model into MainProject.Bussiness/Models/[ControllerName] folder
    - Create View into Views/[ControllerName]/[ActionName]
    - Url: /[Controller]/[Action]

11. Get paging by using below code and replace data of class property in Model:
    @Html.RenderPaging(Model.PagingModel, new MainProject.Framework.Models.PagingStyleModel
    {
        DivContainerStyle = "class=pagination",
        CurrentItemStyle = "class=active"
    })

12. Get Social Page in SettingHelper.GetValueSetting()

13. Get data for Menu:
    - Build Menu in MainProject.Bussiness/Helpers/MenuHelper.
    - Ask Manager to create environment for filling data.

- Admin Site Rule Coding:
1. Open Controllers in MainProject.Bussiness/Admin
    - Url access this is [Domain]/Admin/[ControllerName]
    - Commend the lines [AuthorizeUserAttribute] if you want to use this function
            ** Before commit code plz remove commend of this line.

2. Admin menu in MainProject.Web/Areas/Admin/Views/Shared/_Menu.cshtml
    - In case you create new Entity or Controllers, just add new Menu in here.
    - Add Permission to EntityManageTypeCollection

3. Modify database put the modified query file into folder App_Data.