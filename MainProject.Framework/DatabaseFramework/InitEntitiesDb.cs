﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using System.Web.Security;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Constant;
using MainProject.Framework.Helpers;
using MainProject.Infrastructure.Helpers;
using WebMatrix.WebData;

namespace MainProject.Framework.DatabaseFramework
{
    public class InitEntitiesDb : IDatabaseInitializer<MainDbContext>
    {
        public InitEntitiesDb()
        {
            if (JwtHelper.ValidateToken())
            {
                InitializeDatabase(new MainDbContext());
            }
        }

        #region IDatabaseInitializer<> Members

        public void InitializeDatabase(MainDbContext context)
        {
            bool dbExists;
            using (new TransactionScope(TransactionScopeOption.Suppress))
            {
                dbExists = context.Database.Exists();
            }
            if (dbExists)
            {
                // create all tables
                //var dbCreationScript = ((IObjectContextAdapter)context).ObjectContext.CreateDatabaseScript();
                //context.Database.ExecuteSqlCommand(dbCreationScript);

                //create membership tables
                if (!WebSecurity.Initialized)
                {
                    InitUserDatabase();
                }

                try
                {
                    SeedEntities(context);
                }
                catch (Exception)
                {
                }
            }
            else
            {
                throw new ApplicationException("No database instance");
            }
        }

        public void InitUserDatabase()
        {
            if (!WebSecurity.Initialized)
            {
                WebSecurity.InitializeDatabaseConnection("MainDbContext",
                    "UserProfile", "UserId", "UserName", autoCreateTables: true);
                SeedMembership();
            }
        }

        #endregion

        #region Methods

        public static void SeedEntities(MainDbContext context)
        {
            // Create Resource Key data
            var enumValues = Enum.GetValues(typeof(ResourceKeyCollection));
            if (enumValues.Length != context.StringResourceKeys.Count())
            {
                var languages = context.Languages.Select(x => x.Id).ToList();
                foreach (Enum enumValue in enumValues)
                {
                    string value = enumValue.ToString();
                    if (!context.StringResourceKeys.Any(x => x.Name.Equals(value)))
                    {
                        var entity = new StringResourceKey() { Name = value };
                        context.StringResourceKeys.Add(entity);
                        foreach (var languageId in languages)
                        {
                            context.StringResourceValues.Add(new StringResourceValue()
                            {
                                Key = entity,
                                Value = value,
                                Language = context.Languages.FirstOrDefault(x => x.Id == languageId)
                            });
                        }
                    }
                }

                context.SaveChanges();
            }

            // Seed Section items
            enumValues = Enum.GetValues(typeof(SectionTypeCollection));
            if (enumValues.Length != context.Sections.Count(x => x.Language.Id == 1))
            {
                var languages = context.Languages.Select(x => x.Id).ToList();
                foreach (var enumValue in enumValues)
                {
                    if (!context.Sections.Any(x => x.Order == (SectionTypeCollection)enumValue))
                    {
                        foreach (var languageId in languages)
                        {
                            context.Sections.Add(new Section()
                            {
                                Order = (SectionTypeCollection)enumValue,
                                ImageFolder = FolderHelper.Generate(FolderConstant.Section),
                                Language = context.Languages.FirstOrDefault(x => x.Id == languageId)
                            });
                        }
                    }
                }

                context.SaveChanges();
            }
        }

        public static void SeedMembership()
        {
            var roles = (SimpleRoleProvider)Roles.Provider;
            //var membership = (SimpleMembershipProvider)Membership.Provider;

            if (!roles.RoleExists(RoleName.Admin))
            {
                roles.CreateRole(RoleName.Admin);
            }
            //if (!roles.RoleExists(RoleName.Mod))
            //{
            //    roles.CreateRole(RoleName.Mod);
            //}
            //if (!roles.RoleExists(RoleName.Guest))
            //{
            //    roles.CreateRole(RoleName.Guest);
            //}
            //membership.DeleteAccount(StringConstant.DefaultAdministrator);
            //membership.DeleteUser(StringConstant.DefaultAdministrator, true);
            //membership.CreateUserAndAccount(StringConstant.DefaultAdministrator, "@123456",
            //        new Dictionary<string, object>
            //        {
            //            {"IsActive", true}
            //        });
            //if (!roles.GetRolesForUser(StringConstant.DefaultAdministrator).ToList().Contains(RoleName.Admin))
            //{
            //    roles.AddUsersToRoles(new[] { StringConstant.DefaultAdministrator }, new[] { RoleName.Admin });
            //}
        }
        #endregion
    }
}