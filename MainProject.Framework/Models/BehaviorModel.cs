﻿using System;

namespace MainProject.Framework.Models
{
    public class BehaviorModel
    {
        public string Ip { get; set; }

        public string Url { get; set; }

        public string Source { get; set; }

        public string Device { get; set; }

        public string Website { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}