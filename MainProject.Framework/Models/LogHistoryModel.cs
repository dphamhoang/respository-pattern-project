﻿using MainProject.Core.Enums;
using System;

namespace MainProject.Framework.Models
{
    public class LogHistoryModel
    {
        public long EntityId { get; set; }

        public ActionTypeCollection ActionType { get; set; }

        public string Comment { get; set; }
    }
}
