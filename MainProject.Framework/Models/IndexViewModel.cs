﻿using MainProject.Infrastructure.Models;
using System.Collections.Generic;

namespace MainProject.Framework.Models
{
    public class IndexViewModel<T> where T : class
    {
        public IList<T> ListItems { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public PagingModel PagingViewModel { get; set; }
        
    }
}