﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MainProject.Framework.Models
{
    public class FilterViewModel
    {
        public LanguageSelectModel LanguageViewModel { get; set; }

        public FatherSelectModel FatherSelectModel { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public string BaseUrl { get; set; }

        public FilterViewModel() { }
    }

    public class LanguageSelectModel
    {
        public int LanguageSelectedValue { get; set; }

        public List<SelectListItem> Languages { get; set; }
    }

    public class FatherSelectModel
    {
        public long FatherSelectedValue { get; set; }

        public string FatherSelectedStringValue { get; set; }

        public List<SelectListItem> Fathers { get; set; }
    }
}