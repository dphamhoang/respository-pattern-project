﻿namespace MainProject.Framework.Constant
{
    public static class FolderConstant
    {
        public const string Category = "/Upload/Category/";

        public const string Gallery = "/Upload/Gallery/";

        public const string Article = "/Upload/Article/";

        public const string Brand = "/Upload/Brand/";

        public const string Section = "/Upload/Section/";

        public const string Banner = "/Upload/Banner";

        public const string User = "/Upload/User";
    }
}
