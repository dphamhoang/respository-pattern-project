﻿namespace MainProject.Framework.Constant
{
    public static class CacheConstant
    {
        const string _resourceKey = "Resource";
        const string _shippingKey = "Shipping";

        public static string GetResourceKey(string resourceKey, string culture)
        {
            return string.Format("{0}.{1}.{2}", _resourceKey, culture.ToLower(), resourceKey.ToLower());
        }

        public static string GetShippingKey(string key)
        {
            return string.Format("{0}.{1}", _shippingKey, key.ToLower());
        }
    }
}
