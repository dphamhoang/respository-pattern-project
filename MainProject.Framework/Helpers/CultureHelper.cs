﻿using MainProject.Framework.DatabaseFramework;
using MainProject.Infrastructure.Helpers;
using System.Linq;
using System;

namespace MainProject.Framework.Helpers
{
    public static class CultureHelper
    {
        public static string GetCurrentLanguage()
        {
            var currentLanguage = string.Empty;
            try
            {
                currentLanguage = SessionHelper.Read<string>(SessionItemKey.LanguageKey);
            }
            catch (Exception) { }

            if (string.IsNullOrEmpty(currentLanguage))
            {
                var dbContext = DalHelper.InvokeDbContext();
                var firstLanguage = dbContext.Languages.FirstOrDefault();
                if (firstLanguage == null)
                {
                    InitEntitiesDb.SeedEntities(dbContext);
                }
                currentLanguage = firstLanguage.Key;
                SaveCurrentLanguage(currentLanguage);
            }

            return currentLanguage;
        }

        public static int GetCurrentLanguageId()
        {
            var culKey = GetCurrentLanguage();
            var dbContext = DalHelper.InvokeDbContext();
            var language =
               dbContext.Languages.FirstOrDefault(
                   c => c.Key.Equals(culKey, StringComparison.OrdinalIgnoreCase));
            if (language == null)
            {
                throw new Exception("Please recreate database, there are no language on your data!");
            }
            return language.Id;
        }

        public static void SaveCurrentLanguage(string culture)
        {
            var dbContext = DalHelper.InvokeDbContext();
            var language =
                dbContext.Languages.FirstOrDefault(
                    c => c.Key.Equals(culture, StringComparison.OrdinalIgnoreCase));
            if (language == null)
            {
                language = dbContext.Languages.FirstOrDefault();
                if (language == null)
                {
                    throw new Exception("Please recreate database, there are no language on your data!");
                }
            }
            SessionHelper.Write(SessionItemKey.LanguageKey, language.Key);
        }
    }
}
