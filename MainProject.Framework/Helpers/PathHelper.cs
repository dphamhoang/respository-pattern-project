﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace MainProject.Framework.Helpers
{
    public static class PathHelper
    {
        /// <summary>
        /// Get map path
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string MapPath(string url) => HttpContext.Current.Server.MapPath(url);

        public static string GetUrlEncode(string s) => HttpContext.Current.Server.UrlEncode(s);

        public static string GetRelativePath(string path)
            => path.Replace(MapPath("~/"), "/").Replace(@"\", "/");
    }

    public class FolderHelper
    {
        /// <summary>
        /// Check folder exist
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public static bool Exists(string folderName)
            => Directory.Exists(HttpContext.Current.Server.MapPath(folderName));

        /// <summary>
        /// Delete folder and everything contain in folder
        /// </summary>
        /// <param name="folderName"></param>
        public static void Delete(string folderName)
        {
            if (Exists(folderName))
            {
                Directory.Delete(PathHelper.MapPath(folderName), true);
            }
        }

        /// <summary>
        /// Create folder
        /// </summary>
        /// <param name="folder"></param>
        public static void Create(string folder)
            => Directory.CreateDirectory(PathHelper.MapPath(folder));


        /// <summary>
        /// Generate Folder
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string Generate(string url)
        {
            var folder = url + Guid.NewGuid().ToString();
            while (Exists(folder))
            {
                folder = url + Guid.NewGuid().ToString();
            }

            return folder;
        }
    }

    public class FileHelper
    {
        static string[] _imageExstensions = { ".jpg", ".jpeg", ".gif", ".png", ".svg" };
        static string[] _archieveExtenstion = { ".rar", ".zip", ".7z" };
        static string[] _officeExtension = { ".pdf", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx" };
        static string[] _videoExtension = { ".mp3", ".mp4", ".wmv", ".avi" };

        public static bool Exists(string path) => File.Exists(PathHelper.MapPath(path));

        public static void Delete(string path) => File.Delete(PathHelper.MapPath(path));

        public static byte[] ReadAllBytes(string url) => File.ReadAllBytes(PathHelper.MapPath(url));

        public static string SaveFile(string folder, HttpPostedFileBase file)
        {
            try
            {
                if (_imageExstensions.Contains(Path.GetExtension(file.FileName)))
                {
                    if (!FolderHelper.Exists(folder)) FolderHelper.Create(folder);

                    // Get physical file path on disc Ex: C:\inetpub\wwwroot\file.extension
                    string physicalFilePath = GetFilePath(folder, file.FileName);
                    //save file
                    file.SaveAs(physicalFilePath);

                    return PathHelper.GetRelativePath(physicalFilePath);
                }
            }
            catch (Exception) { }

            return null;
        }

        /// <summary>
        /// Get file name incase it is duplicated
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="file">file.extenstion</param>
        /// <returns></returns>
        public static string GetFilePath(string folder, string file)
        {
            string name = Path.GetFileNameWithoutExtension(file);
            var imgPath = PathHelper.MapPath(folder + "/" + name + Path.GetExtension(file));
            while (File.Exists(imgPath))
            {
                if (name.Contains("-"))
                {
                    int n;
                    var str = name.Substring(name.LastIndexOf("-") + 1);
                    if (int.TryParse(str, out n))
                    {
                        n++;
                        name = name.Substring(0, name.LastIndexOf("-") + 1) + n;
                    }
                    else
                    {
                        name = name + "-1";
                    }
                }
                else
                {
                    name = name + "-1";
                }
                imgPath = PathHelper.MapPath(folder + "/" + name + Path.GetExtension(file));
            }
            return imgPath;
        }

        /// <summary>
        /// Get all images in Folder
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        public static List<string> GetImages(string folder)
        {
            var result = new List<string>();
            if (!string.IsNullOrEmpty(folder) && Directory.Exists(PathHelper.MapPath(folder)))
            {
                var directory = new DirectoryInfo(PathHelper.MapPath(folder));
                FileInfo[] files = directory.GetFiles();
                foreach (FileInfo file in files)
                {
                    if (_imageExstensions.Contains(file.Extension.ToLower()))
                    {
                        result.Add($"{folder}/{file.Name}");
                    }
                }
            }
            return result;
        }

        public static bool IsValidFileType(string fileName)
        {
            string extension = Path.GetExtension(fileName);
            if (string.IsNullOrWhiteSpace(extension) || extension.Length < 1)
                return false;

            return _imageExstensions.Contains(extension, StringComparer.OrdinalIgnoreCase)
                || _archieveExtenstion.Contains(extension, StringComparer.OrdinalIgnoreCase)
                || _officeExtension.Contains(extension, StringComparer.OrdinalIgnoreCase)
                || _videoExtension.Contains(extension, StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets an image from the specified URL.
        /// </summary>
        /// <param name="url">The URL containing an image.</param>
        /// <returns>The image as a bitmap.</returns>
        public static Bitmap GetImage(string url)
        {
            var buffer = 1024;
            Bitmap image = null;

            if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                return image;

            using (var ms = new MemoryStream())
            {
                var req = WebRequest.Create(url);

                using (var resp = req.GetResponse())
                {
                    using (var stream = resp.GetResponseStream())
                    {
                        var bytes = new byte[buffer];
                        var n = 0;

                        while ((n = stream.Read(bytes, 0, buffer)) != 0)
                            ms.Write(bytes, 0, n);
                    }
                }

                image = Bitmap.FromStream(ms) as Bitmap;
            }

            return image;
        }

        public static void WriteTransaction(string fileName, string message)
        {
            string folder = "/Upload/Transaction";
            if (!FolderHelper.Exists(folder))
            {
                FolderHelper.Create(folder);
            }
            File.AppendAllText(PathHelper.MapPath(string.Format("{0}/{1}-{2}.txt", folder, fileName, DateTime.Now.ToString("ddMMyyyy"))),
                               string.Format("{0}: {1}{2}", DateTime.Now.ToString("HH:mm"), message, Environment.NewLine));
        }
    }
}
