﻿using MainProject.Infrastructure.Constant;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net;
using System;

namespace MainProject.Framework.Helpers
{
    public class MailHelper
    {
        /// <summary>
        /// Send a mail to specify email and cc to other emails in setting entity
        /// </summary>
        /// <param name="email">Recipent email</param>
        /// <param name="body">Mail content</param>
        /// <param name="subject">Mail subject</param>
        /// <param name="emails">Additional cc emails</param>
        /// <param name="filesToAttach">files</param>
        public static void Send(string email, string body, string subject, string[] emails)
        {
            try
            {
                var settings = SettingHelper.GetSettingValueByListKey(
                                        new List<string>(){ MailConstant.Server, MailConstant.Port, MailConstant.SSL,
                                                MailConstant.UserName, MailConstant.Password, MailConstant.EmailCC });

                // Create a message and set up the recipients.
                MailMessage message = new MailMessage(settings[MailConstant.UserName], email, subject, body);
                message.IsBodyHtml = true;
                foreach (var emailCC in settings[MailConstant.EmailCC].Split(';'))
                {
                    message.CC.Add(emailCC);
                }
                // Add additional cc emails
                if (emails != null)
                {
                    foreach (var emailCC in emails)
                    {
                        message.CC.Add(emailCC);
                    }
                }

                //Send the message.
                SmtpClient client = new SmtpClient(settings[MailConstant.Server], Convert.ToInt32(settings[MailConstant.Port]));
                client.EnableSsl = Convert.ToBoolean(settings[MailConstant.SSL]);
                // Add credentials if the SMTP server requires them.
                client.Credentials = new NetworkCredential(settings[MailConstant.UserName], settings[MailConstant.Password]);

                client.Send(message);
            }
            catch (Exception) { }
        }

    }
}
