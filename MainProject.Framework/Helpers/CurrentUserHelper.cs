﻿using System.Web;
using MainProject.Core.Enums;
using MainProject.Data.Repositories;

namespace MainProject.Framework.Helpers
{
    public class CurrentUserHelper
    {
        private RoleRepository _roleRepository;
        private HttpRequestBase _request;

        public CurrentUserHelper(HttpRequestBase request)
        {
            var db = DalHelper.InvokeDbContext();
            _roleRepository = new RoleRepository(db);
            _request = request;
        }

        public bool CheckHasPermissionOnFeature(EntityManageTypeCollection feature)
        {
            if (!_request.IsAuthenticated) return false;

            return _roleRepository.VerifyUserPermissionOfFeature(AccountHelper.CurrentUserName(), feature);
        }

        public bool CheckHasPermissionOnFeature(EntityManageTypeCollection feature, PermissionCollection permission)
        {
            if (!_request.IsAuthenticated) return false;

            return _roleRepository.VerifyUserPermissionOfAction(AccountHelper.CurrentUserName(), permission, feature);
        }

        public bool IsCurrentUser(string userName)
            => AccountHelper.CurrentUserName().Equals(userName, System.StringComparison.OrdinalIgnoreCase);
    }
}
