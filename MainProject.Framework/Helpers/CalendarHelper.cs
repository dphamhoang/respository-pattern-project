﻿using System;

namespace MainProject.Framework.Helpers
{
    public static class CalendarHelper
    {
        public static string Convert(DateTime dateTime)
            => string.Format("{0} tháng {1} - {2}", dateTime.Day, dateTime.Month, dateTime.Year);

        public static string ConvertDayOfWeek(DateTime dateTime)
            => string.Format("{0}, {1}", DayName(dateTime.DayOfWeek.ToString()), dateTime.ToString("dd/MM/yyyy"));

        private static string DayName(string day)
        {
            switch(day)
            {
                case "Monday":
                    return "Thứ hai";
                case "Tuesday":
                    return "Thứ ba";
                case "Wednesday":
                    return "Thứ tư";
                case "Thursday":
                    return "Thứ năm";
                case "Friday":
                    return "Thứ sáu";
                case "Saturday":
                    return "Thứ bảy";
                default:
                    return "Chủ nhật";
            }
        }
    }
}
