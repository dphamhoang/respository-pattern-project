﻿using MainProject.Framework.Models;
using System.Net;
using System;
using MainProject.Infrastructure.Helpers;

namespace MainProject.Framework.Helpers
{
    public class ExternalValidationHelper
    {
        //private static string fbAppToken = "1260704607376976|-B4mL-s7Dbs_I2ZQE4STo2_2Qog";
        //public static bool IsValidGmailToken(string token)
        //{
        //    WebClient client = new WebClient();

        //    var response = client.DownloadString("https://www.googleapis.com/oauth2/v1/tokeninfo?id_token=" + token);

        //    if (!string.IsNullOrEmpty(JsonConvert.DeserializeObject<GoogleResponse>(response).Error))
        //    {
        //        return false;
        //    }

        //    return true;
        //}

        //public static bool IsValidFacebookToken(string token)
        //{
        //    WebClient client = new WebClient();

        //    var response = client.DownloadString("https://graph.facebook.com/v4.0/debug_token?input_token=" + token + "&access_token=" + fbAppToken);

        //    if (!JsonConvert.DeserializeObject<FacebookResponse>(response).Data.IsValid)
        //    {
        //        return false;
        //    }

        //    return true;
        //}

        public static void Post(string url, string query)
        {
            try
            {
                string ip = AuthorizationHelper.GetIP();
                if (!ip.Equals("::1"))
                {
                    WebClient wc = new WebClient();
                    wc.Headers["Content-Type"] = "application/json; charset=utf-8";
                    wc.UploadString("http://web6.web4gsolutions.com/api/behavior/post", JsonHelper.Serialize(new BehaviorModel()
                    {
                        Ip = ip,
                        CreatedDate = DateTime.Now,
                        Source = query.Contains("utm_source") || query.Contains("gclid") || query.Contains("fbclid") ? query : "",
                        Device = AuthorizationHelper.DetectUserAgent(),
                        Url = url,
                        Website = "Hana"
                    }));
                }
            }
            catch (Exception) { }
        }
    }
}
