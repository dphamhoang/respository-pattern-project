﻿using MainProject.Core.Enums;
using MainProject.Core;
using System;
using MainProject.Data;
using System.Linq;

namespace MainProject.Framework.Helpers
{
    public static class EntityHelper
    {
        public static object GetUrlRecord(this MainDbContext dbContext, string urlWithoutParam)
            => dbContext.UrlRecords.FirstOrDefault(c => c.Url.Equals(urlWithoutParam));

        public static bool MyRedirect(Infrastructure.Models.RouterModel model, out string controllerName, out string actionName, out long id)
        {
            var urlRecord = (UrlRecord)model.UrlRecord;

            controllerName = string.Empty;
            actionName = string.Empty;
            id = urlRecord.EntityId;
            // Switch language by url
            CultureHelper.SaveCurrentLanguage(urlRecord.Language.Key);

            switch (
                (EntityTypeCollection)
                Enum.Parse(typeof(EntityTypeCollection), urlRecord.EntityType.ToString()))
            {
                case EntityTypeCollection.Articles:
                    controllerName = "Home";
                    actionName = "Article";
                    break;
                case EntityTypeCollection.Categories:
                    controllerName = "Home";
                    actionName = "Category";
                    break;
                default:
                    break;
            }

            if (!string.IsNullOrEmpty(controllerName) && !string.IsNullOrEmpty(actionName))
                return true;

            return false;
        }
    }
}
