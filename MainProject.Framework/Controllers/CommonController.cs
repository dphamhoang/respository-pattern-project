﻿using MainProject.Infrastructure.Configs;
using MainProject.Infrastructure.Helpers;
using MainProject.Framework.Helpers;
using System.Web.Mvc;
using System.Linq;
using System;

namespace MainProject.Framework.Controllers
{
    public class CommonController : BaseController
    {
        public CommonController()
        {
        }

        public ActionResult ChangeCulture(string culture, string url)
        {
            var newCurrentLanguage = DbContext.Languages.FirstOrDefault(c => c.Key.Equals(culture, StringComparison.OrdinalIgnoreCase));
            if (newCurrentLanguage == null)
            {
                return Redirect("/");
            }
            CultureHelper.SaveCurrentLanguage(newCurrentLanguage.Key);

            //var routerModel = UrlRecordHelper.GetUrlRecordFromUrl(WebUtility.UrlDecode(url));
            return Redirect("/");
        }

        public ActionResult Redirect()
        {
            var request = Request.Url.PathAndQuery;
            if (request.Equals("/_common-settings/ReGenerateDb", StringComparison.OrdinalIgnoreCase))
            {
                return
                    new MVCTransferResult(
                        new
                        {
                            controller = "Service",
                            action = "ReGenerateDb"
                        });
            }
            // Ecommerce prevent customer account can pass maintain page
            //else if (ConfigItemHelper.IsMaintainance() && !AccountHelper.IsAdmin())
            else if (ConfigItemHelper.IsMaintainance() && !Request.IsAuthenticated)
            {
                return
                    new MVCTransferResult(
                        new
                        {
                            controller = "Home",
                            action = "Maintainance"
                        });
            }
            if (request.StartsWith("/_common-settings/ChangeCulture", StringComparison.OrdinalIgnoreCase))
            {
                return RedirectHelper.ChangeCulture(request);
            }
            else
            {
                if (RedirectHelper.Verify(request))
                {
                    var dbContext = DalHelper.InitDbContext();
                    DalHelper.SaveDbContextToRequest(dbContext);

                    return RedirectHelper.Redirect(request, Request.Url.AbsolutePath, dbContext.GetUrlRecord, EntityHelper.MyRedirect);
                }
            }

            return new MVCTransferResult(new { controller = "Home", action = "Index" });
        }
    }
}
