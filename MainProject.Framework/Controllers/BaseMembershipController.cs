﻿using System.Web.Security;
using WebMatrix.WebData;

namespace MainProject.Framework.Controllers
{
    public class BaseMembershipController : BaseController
    {
        public SimpleRoleProvider RoleProvider
        {
            get
            {
                return (SimpleRoleProvider)Roles.Provider;
            }
        }

        public SimpleMembershipProvider MembershipProvider
        {
            get
            {
                return (SimpleMembershipProvider)Membership.Provider;
            }
        }
    }
}
