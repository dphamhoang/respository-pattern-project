﻿namespace MainProject.Bussiness.Models
{
    public class HeaderViewModel
    {
        public string MainMenu { get; set; }

        public string MainMenuMobile { get; set; }

        public string CurrentLanguageKey { get; set; }
    }

    public class FooterViewModel
    {
        public string MainMenu { get; set; }
    }
}