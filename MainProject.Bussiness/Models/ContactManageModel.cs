﻿using System.ComponentModel.DataAnnotations;

namespace MainProject.Bussiness.Models
{
    public class ContactManageModel
    {
        [StringLength(200, ErrorMessage = "Contact_Name_Length")]
        [Required(ErrorMessage = "Contact_Name_Required")]
        public string Name { get; set; }

        [RegularExpression("^[0-9]{10,11}$", ErrorMessage = "Contact_Phone_Length")]
        [Required(ErrorMessage = "Contact_Phone_Required")]
        public string Phone { get; set; }

        [StringLength(200, ErrorMessage = "Email_Length")]
        [Required(ErrorMessage = "Email_Required")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email_Regex")]
        public string Email { get; set; }

        //[StringLength(200, ErrorMessage = "Chieu_dai_khong_duoc_qua_200_ky_tu")]
        //[Required(ErrorMessage = "Chua_nhap_dia_chi")]
        //public string Address { get; set; }

        [StringLength(4000, ErrorMessage = "Contact_Content_Length")]
        [Required(ErrorMessage = "Contact_Content_Required")]
        public string Content { get; set; }

        public static void ToEntity(ContactManageModel model , ref Core.Contact entity)
        {
            entity.Name = model.Name;
            entity.Email = model.Email;
            entity.Phone = model.Phone;
            //entity.Address = model.Address;
            entity.Content = model.Content;
            //if (uTM != null)
            //{
            //    entity.Source = uTM.Source;
            //    entity.Term = uTM.Term;
            //    entity.Medium = uTM.Medium;
            //    entity.Campaign = uTM.Campaign;
            //    entity.UTMContent = uTM.UTMContent;
            //}
        }
    }
}