﻿using System.Collections.Generic;
using MainProject.Core;
using MainProject.Framework.Models;
using MainProject.Infrastructure.Models;

namespace MainProject.Bussiness.Models
{
    public class FaqItemsViewModel
    {
        public List<FaqItem> ListFaqItem { get; set; }
        public PagingModel PagingViewModel { get; set; }

        //Added by MK
        public Category Category { get; set; }
    }
}