﻿using MainProject.Core;
using System;
using System.Collections.Generic;

namespace MainProject.Bussiness.Models
{
    public class GalleryViewModel
    {
        public Category Category { get; set; }

        public List<GalleryItemViewModel> Albums { get; set; }

        public int CurrentPage { get; set; }

        public bool IsCanLoadMore { get; set; }
    }

    public class GalleryItemViewModel
    {
        public string Name { get; set; }

        public string Path { get; set; }

        public DateTime Order { get; set; }

        public List<Media> Medias { get; set; }
    }
}