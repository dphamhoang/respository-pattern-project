﻿using System;
using System.Collections.Generic;
using MainProject.Core;

namespace MainProject.Bussiness.Models
{
    public class SitemapModel
    {
        public string Link { get; set; }
        public DateTime Created { get; set; }
        public string Priority { get; set; }
        public string Title { get; set; }
        public string Changefreg { get; set; } = "weekly";
    }
}