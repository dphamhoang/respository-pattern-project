﻿using System.Collections.Generic;
using MainProject.Core;
using MainProject.Infrastructure.Models;

namespace MainProject.Bussiness.Models
{
    /// <summary>
    /// Search page model 
    /// </summary>
    public class SearchItemViewModel
    {
        // Home page slides
        public List<Article> Items { get; set; }

        // Paging model
        public PagingModel PagingModel { get; set; }

        // Search text
        public string Text { get; set; }

        // Total results
        public int TotalResults { get; set; }
    }
}