﻿using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Data.Repositories;
using MainProject.Bussiness.Models;
using System.Linq;

namespace MainProject.Bussiness.Services
{
    public class GalleryService
    {
        #region Fields
        private readonly AlbumRepository _albumRepository;
        private readonly MediaRepository _mediaRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly int _itemPerPage = 9; 
        #endregion

        public GalleryService(MainDbContext dbContext)
        {
            _albumRepository = new AlbumRepository(dbContext);
            _mediaRepository = new MediaRepository(dbContext);
            _categoryRepository = new CategoryRepository(dbContext);
        }

        public GalleryViewModel Get(int languageId, bool isVideo, int page = 1)
        {
            if (page < 1) page = 1;
            var category = 
                _categoryRepository.FindUnique(
                        x => x.DisplayTemplate == (isVideo ? DisplayTemplateCollection.Video 
                                : DisplayTemplateCollection.Image) && x.Language.Id == languageId);
            var query = _albumRepository.Find(x => x.IsVideo == isVideo && x.Language.Id == languageId && x.IsPublished);

            return new GalleryViewModel() {
                Category = category,
                Albums = query.OrderBy(x => x.Order).Skip((page - 1) * _itemPerPage).Take(_itemPerPage).ToList()
                                 .Select(x => new GalleryItemViewModel() {
                                     Path = x.ResourcePath, Name = x.Name, Order = x.Order,
                                     Medias = _mediaRepository.Get(x.CreatedDate).OrderBy(y => y.Order).ToList()
                                 }).ToList(),
                IsCanLoadMore = query.Count() > (page * _itemPerPage + page),
                CurrentPage = page + 1,
            };
        }
    }
}
