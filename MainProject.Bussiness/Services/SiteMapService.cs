﻿using MainProject.Data;
using MainProject.Data.Repositories;
using MainProject.Bussiness.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MainProject.Service.LandingPage.Services
{
    public class SiteMapService
    {
        private readonly ArticleRepository _articleRepository;
        //private readonly ProductRepository _productRepository;
        private readonly CategoryRepository _categoryRepository;
        //private readonly CommerceCategoryRepository _commerceCategoryRepository;

        public SiteMapService(MainDbContext dbContext)
        {
            _articleRepository = new ArticleRepository(dbContext);
            //_productRepository = new ProductRepository(dbContext);
            _categoryRepository = new CategoryRepository(dbContext);
            //_commerceCategoryRepository = new CommerceCategoryRepository(dbContext);
        }



        public List<SitemapModel> GetSitemap()
        {
            var results = new List<SitemapModel>();
            var categories = _categoryRepository.FindAll().ToList();
            foreach (var item in categories)
            {
                if (item.Parent == null)
                {
                    results.Add(new SitemapModel
                    {
                        Title = item.Title,
                        Link = item.GetPrefixUrl(),
                        Created = DateTime.Now,
                        Priority = "0.20"
                    });
                }
                else
                {
                    results.Add(new SitemapModel
                    {
                        Title = item.Title,
                        Link = item.GetPrefixUrl(),
                        Created = DateTime.Now,
                        Priority = "0.20"
                    });
                }
            }

            var articles = _articleRepository.FindAll().ToList();
            foreach (var item in articles)
            {
                results.Add(new SitemapModel
                {
                    Title = item.Title,
                    Link = item.GetUrl(),
                    Created = DateTime.Now,
                    Priority = "1.00"
                });
            }

            //var commerceCategories = _commerceCategoryRepository.GetAll().ToList();
            //foreach (var item in commerceCategories)
            //{
            //    results.Add(new SitemapModel
            //    {
            //        Title = item.Name,
            //        Link = item.GetPrefixUrl(),
            //        Created = DateTime.Now,
            //        Priority = "0.60"
            //    });
            //}

            //var products = _productRepository.FindAll().ToList();
            //foreach (var item in products)
            //{
            //    results.Add(new SitemapModel
            //    {
            //        Title = item.Name,
            //        Link = item.GetUrl(),
            //        Created = DateTime.Now,
            //        Priority = "1.00"
            //    });
            //}

            return results;
        }
    }
}
