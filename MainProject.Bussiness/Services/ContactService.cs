﻿using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Data.Repositories;
using MainProject.Framework.Helpers;
using MainProject.Infrastructure.Models;
using MainProject.Bussiness.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace MainProject.Bussiness.Services
{
    public class ContactService
    {
        private readonly ContactRepository _contactRepository;
        public ContactService(MainDbContext dbContext)
        {
            _contactRepository = new ContactRepository(dbContext);
        }

        public BaseResponseModel Save(bool modelState, string message, string recaptcha, ContactManageModel model)
        {
            if (modelState)
            {
                //secret that was generated in key value pair
                string secret = SettingHelper.GetValueSetting("SecretKeyRecaptcha");
                var client = new WebClient();
                var reply =
                    client.DownloadString(
                        string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret,
                            recaptcha));

                var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);

                //when response is true check for the error message
                if (captchaResponse.Success)
                {
                    // Bind data model to entity
                    var entity = new Core.Contact
                    {
                        CreatedDate = DateTime.Now
                    };
                    ContactManageModel.ToEntity(model, ref entity);
                    // Parse model to entity with UTM
                    //ContactManageModel.ToEntity(model, CookieHelper.Read<UTM>(CookieItemKey.UTM), ref entity);
                    // Insert contact information to db
                    _contactRepository.Insert(entity);
                    // Create template for email
                    #region Email content
                    string body = "<div><h3>Chúng tôi đã nhận được thông tin liên hệ từ bạn với thông tin sau:</h3></div>";
                    body += "<fieldset><legend>Thông tin liên hệ</legend>";
                    body += "<div><b>Họ tên:</b></div>";
                    body += "<div>" + entity.Name + "</div>";
                    body += "<div><b>Điện thoại:</b></div>";
                    body += "<div>" + entity.Phone + "</div>";
                    body += "<div><b>Email:</b></div>";
                    body += "<div>" + entity.Email + "</div>";
                    body += "<div><b>Địa chỉ:</b></div>";
                    body += "<div>" + entity.Address + "</div>";
                    body += "<div><b>Nội dung :</b></div>";
                    body += "<div>" + entity.Content + "</div>";
                    body += "</fieldset>"; 
                    #endregion
                    // Send mail to submitter
                    MailHelper.Send(entity.Email, body, "Xác nhận thông tin liên hệ!", null);

                    return new BaseResponseModel
                    {
                        Code = HttpStatusCode.OK,
                        Message = ResourceHelper.GetResource(ResourceKeyCollection.Contact_Success)
                    };
                }

                switch (captchaResponse.ErrorCodes[0].ToLower())
                {
                    case ("missing-input-secret"):
                        message = string.Format("The secret parameter is missing.");
                        break;
                    case ("invalid-input-secret"):
                        message = string.Format("The secret parameter is invalid or malformed.");
                        break;
                    case ("missing-input-response"):
                        message = string.Format("The response parameter is missing.");
                        break;
                    case ("invalid-input-response"):
                        message = string.Format("The response parameter is invalid or malformed.");
                        break;
                    default:
                        message = string.Format("Error occured. Please try again");
                        break;
                }

            }

            return new BaseResponseModel
            {
                Code = HttpStatusCode.BadRequest,
                Message = message
            };
        }
    }
}
