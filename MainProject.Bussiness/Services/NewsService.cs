﻿using MainProject.Data;
using MainProject.Data.Repositories;

namespace MainProject.Bussiness.Services
{
    public class ArticleService
    {
        private readonly ArticleRepository _articleRepository;

        public ArticleService(MainDbContext dbContext)
        {
            _articleRepository = new ArticleRepository(dbContext);
        }

        public Core.Article GetDetail(long id)
            => _articleRepository.FindId(id);
    }
}
