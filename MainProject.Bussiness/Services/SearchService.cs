﻿using MainProject.Infrastructure.Models;
using MainProject.Data.Repositories;
using MainProject.Bussiness.Models;
using MainProject.Data;
using System.Linq;

namespace MainProject.Bussiness.Services
{
    public class SearchService
    {
        #region Fields
        private readonly ArticleRepository _articleRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly LanguageRepository _languageRepository;
        private readonly int _itemPerPage = 20;
        #endregion
        
        #region Constructor
        public SearchService(MainDbContext DbContext)
        {
            _articleRepository = new ArticleRepository(DbContext);
            _categoryRepository = new CategoryRepository(DbContext);
            _languageRepository = new LanguageRepository(DbContext);
        }
        #endregion

        public SearchItemViewModel Search(string text, int page, int languageId)
        {
            if (page < 1) page = 1;
            var textS = text.Trim().ToLower();

            var query = _articleRepository.Find(
                                x => x.Title.Contains(textS) || x.Description.Contains(textS) || x.Content.Contains(textS)
                                        || x.Language.Id == languageId && x.IsPublished);

            var count = query.Count();

            return new SearchItemViewModel()
            {
                Items = query.OrderByDescending(x => x.Id).Skip((page - 1) * _itemPerPage).Take(_itemPerPage).ToList(),
                PagingModel = new PagingModel(count, _itemPerPage, page, "href='/Search?text=" + text + "&page=" + "{0}'"),
                TotalResults = count,
                Text = text
            };
        }
    }
}
