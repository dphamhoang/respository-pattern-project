﻿using MainProject.Service.LandingPage.Services;
using MainProject.Framework.Controllers;
using System.Web.Mvc;
using System;

namespace MainProject.SBussiness.Controllers
{
    public class SiteMapController : BaseController
    {
        private readonly SiteMapService _service;

        public SiteMapController()
        {
            _service = new SiteMapService(DbContext);
        }

        public PartialViewResult Sitemap()
        {
            var list = _service.GetSitemap();
            Response.AddHeader("Content-Type", "text/xml");
            ViewBag.host = String.Format("http://{0}", Request.Url.Host);
            return PartialView(list);
        }
    }
}
