﻿using MainProject.Bussiness.Services;
using MainProject.Framework.Controllers;
using System.Web.Mvc;

namespace MainProject.Bussiness.Controllers
{
    public class GalleryController : BaseController
    {
        // GET: Gallery
        private readonly GalleryService _service;

        public GalleryController()
        {
            _service = new GalleryService(DbContext);
        }

        public ActionResult Index(bool isVideo)
        {
            var model = _service.Get(CurrentLanguageId, isVideo);

            if (model != null)
            {
                //Set page title
                ViewBag.Title = model.Category.Title;
                //Set Meta data
                ViewBag.MetaTitle = model.Category.MetaTitle;
                ViewBag.MetaDescription = model.Category.MetaDescription;
                ViewBag.MetaKeywords = model.Category.MetaKeywords;
                ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + model.Category.ImageDefault;
                ViewBag.Type = "website";
                ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + model.Category.GetPrefixUrl();
            }

            return View(isVideo ? "Video" : "Image", model);
        }


    }
}