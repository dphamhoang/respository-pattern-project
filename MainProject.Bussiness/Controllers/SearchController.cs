﻿using MainProject.Bussiness.Services;
using MainProject.Framework.Controllers;
using System.Web.Mvc;

namespace MainProject.Bussiness.Controllers
{
    public class SearchController : BaseController
    {
        private readonly SearchService _service;
        public SearchController()
        {
            _service = new SearchService(DbContext);
        }

        [Route("search")]
        public ActionResult Index(string text="", int page=1)
        {
            
            return View();
        }
    }
}