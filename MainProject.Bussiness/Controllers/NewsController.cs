﻿using MainProject.Bussiness.Services;
using MainProject.Framework.Controllers;
using System.Web.Mvc;

namespace MainProject.Bussiness.Controllers
{
    public class NewsController : BaseController
    {
        // GET: Article
        private readonly ArticleService _service;

        public NewsController()
        {
            _service = new ArticleService(DbContext);
        }

        public ActionResult Detail(long id)
        {
            var model = _service.GetDetail(id);

            if (model != null)
            {
                //Set page title
                ViewBag.Title = model.Title;
                //Set Meta data
                ViewBag.MetaTitle = model.MetaTitle;
                ViewBag.MetaDescription = model.MetaDescription;
                ViewBag.MetaKeywords = model.MetaKeywords;
                ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + model.ImageDefault;
                ViewBag.Type = "website";
                ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + model.GetUrl();
            }

            return View(model);
        }
    }
}