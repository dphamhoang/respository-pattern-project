﻿using MainProject.Infrastructure.Configs;
using MainProject.Framework.Controllers;
using MainProject.Bussiness.Services;
using MainProject.Framework.Constant;
using MainProject.Bussiness.Models;
using MainProject.Core.Enums;
using System.Web.Mvc;

namespace MainProject.Bussiness.Controllers
{
    public class HomeController : BaseController
    {
        #region
        private readonly HomeService _service;
        #endregion

        #region Constructors
        public HomeController()
        {
            _service = new HomeService(DbContext);
        }
        #endregion


        public ActionResult Index()
        {
            var model = _service.Get();

            if (model != null)
            {
                //Set page title
                ViewBag.Title = model.Category.Title;
                //Set Meta data
                ViewBag.MetaTitle = model.Category.MetaTitle;
                ViewBag.MetaDescription = model.Category.MetaDescription;
                ViewBag.MetaKeywords = model.Category.MetaKeywords;
                ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + model.Category.ImageDefault;
                ViewBag.Type = "website";
                ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority;
            }

            return View(model);
        }

        // Render the view of header
        public ActionResult ShowHeader() => PartialView("_Header", _service.GetHeader(CurrentLanguageId));
			
		// Render the footer view at the right
        public ActionResult ShowFooter() => PartialView("_Footer", _service.GetFooter(CurrentLanguageId));

        public ActionResult Category(int id)
        {
            var category = _service.GetCategory(id);
            if (category != null)
            {
                if (category.DisplayTemplate == DisplayTemplateCollection.Home)
                {
                    return
                        new MVCTransferResult(
                            new
                            {
                                controller = "Home",
                                action = "Index"
                            });
                }
            }
            return View(StringConstant.ViewForErrorPage);
        }

        public ActionResult Article(int id)
        {
            var entity = _service.GetArticle(id);

            if (entity != null)
            {
                //Added by MK             
                //if (entity.Category.DisplayTemplate == DisplayTemplateCollection.IntroTemplate)
                //{
                //    return
                //        new MVCTransferResult(
                //            new
                //            {
                //                controller = "Intro",
                //                action = "Detail",
                //                id = id
                //            });
                //}
            }

            return View(StringConstant.ViewForErrorPage);
        }

        [Route("dang-ky-nhan-tin")]
        [HttpPost]
        public JsonResult Subscribe(SubscribeModel model)
            => Json(_service.Subscribe(ModelState.IsValid, model));

        public ActionResult Error()
            => View(StringConstant.ViewForErrorPage);

        public ActionResult Maintainance() => View();
    }
}

