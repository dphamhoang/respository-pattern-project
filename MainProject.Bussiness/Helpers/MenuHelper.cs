﻿using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MainProject.Bussiness.Helpers
{
    public static class MenuHelper
    {
        #region LandingPage
        public static string BuildMenu(Menu menu, Language language, MenuItemRepository menuRepository)
        {
            var result = string.Empty;
            if (menu != null)
            {
                var menuItems = menuRepository.FindMenu(menu, language);
                var strListLi = string.Empty;
                foreach (var menuItem in menuItems.Where(x => x.Parent == null))
                {
                    strListLi += BuildMenuItem(menuItem, menuItems);
                }
                result = string.Format("<ul class=\"\">{0}</ul>", strListLi).Replace("target=\"\"", "");
            }
            return result;
        }

        private static string BuildMenuItem(MenuItem mnuItem, List<MenuItem> list)
        {
            var str = string.Empty;
            if (mnuItem != null)
            {
                var linkTarget = mnuItem.LinkTarget == LinkTargetCollection.Self ? "" : mnuItem.LinkTarget.ToString();
                if (list.Any(x => x.Parent != null && x.Parent.Id == mnuItem.Id))
                {
                    var strChilds = string.Empty;
                    foreach (
                        var m in
                            list.Where(x => x.Parent != null && x.Parent.Id == mnuItem.Id)
                                .OrderByDescending(c => c.Order))
                    {
                        strChilds += BuildMenuItem(m, list);
                    }
                    str = string.Format("<li><a href=\"{0}\" target=\"{1}\">{2}</a><ul>{3}</ul></li>", mnuItem.Link, linkTarget, mnuItem.Title, strChilds);
                }
                else
                {
                    str = string.Format("<li><a href=\"{0}\" target=\"{1}\">{2}</a></li>", mnuItem.Link, linkTarget, mnuItem.Title);
                }
            }
            return str;
        }

        public static string BuildFooterMenu(Menu menu, Language language, MenuItemRepository menuRepository)
        {
            var result = string.Empty;
            if (menu != null)
            {
                var menuItems = menuRepository.FindMenu(menu, language);
                var strListLi = string.Empty;
                foreach (var menuItem in menuItems.Where(x => x.Parent == null).OrderBy(x => x.Order))
                {
                    var level = 0;
                    strListLi += string.Format("<div class=\"item-mainft\"><h3>{0}</h3><ul>{1}</ul></div>"
                                                , menuItem.Title
                                                , BuildFooterMenuItem(menuItem, menuItems, ref level));
                }

                result = string.Format("{0}", strListLi).Replace("target=\"\"", "");
            }
            return result;
        }

        private static string BuildFooterMenuItem(MenuItem mnuItem, List<MenuItem> list, ref int level)
        {
            level++;
            var str = string.Empty;
            if (mnuItem != null)
            {
                var linkTarget = mnuItem.LinkTarget == LinkTargetCollection.Self ? "" : mnuItem.LinkTarget.ToString();
                if (list.Any(x => x.Parent != null && x.Parent.Id == mnuItem.Id))
                {

                    var strChilds = string.Empty;
                    foreach (var m in list.Where(x => x.Parent != null && x.Parent.Id == mnuItem.Id).OrderBy(c => c.Order))
                    {
                        strChilds += BuildFooterMenuItem(m, list, ref level);
                        level--;
                    }
                    str = strChilds;
                }
                else
                {
                    if (level != 1)
                        str = string.Format("<li><a href=\"{0}\" target=\"{1}\">{2}</a></li>", !string.IsNullOrEmpty(mnuItem.Link) ? mnuItem.Link : "javascript:void(0)", linkTarget, mnuItem.Title);
                }
            }
            return str;
        }
        #endregion

        public static List<SelectListItem> BindDropdown(IList<Menu> menus, long selectedvalue)
        {
            return menus.Select(
                d => new SelectListItem
                {
                    Text = d.CodeName,
                    Value = d.Id.ToString(),
                    Selected = selectedvalue == d.Id
                }).ToList();
        }
    }
}
