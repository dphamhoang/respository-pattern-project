﻿using MainProject.Core.Enums;
using MainProject.Framework.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MainProject.Bussiness.Helpers
{
   public class EnumBidingHelper
    {
        public static List<SelectListItem> LinkTargetSelectListItems(LinkTargetCollection? selectedvalue)
        {
            return Enum.GetValues(typeof(LinkTargetCollection)).Cast<LinkTargetCollection>()
                .Select(c => new SelectListItem
                {
                    Value = c.ToString(),
                    Text = c.GetDescription(),
                    Selected = selectedvalue == c
                }).ToList();
        }
    }
}
