﻿using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Framework.Helpers;
using MainProject.Infrastructure.Helpers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MainProject.Bussiness.Admin.Models
{
    public class SectionManageModel
    {
        public int Id { get; set; }

        [StringLength(1000, ErrorMessage = "Không được vượt quá 1000 ký tự")]
        public string Title { get; set; }

        [AllowHtml]
        public string Content { get; set; }

        [StringLength(4000, ErrorMessage = "Không được vượt quá 4000 ký tự")]
        public string Image { get; set; }

        [StringLength(1000, ErrorMessage = "Không được vượt quá 1000 ký tự")]
        public string ImageFolder { get; set; }

        public SectionTypeCollection Order { get; set; }

        public bool IsMedia { get; set; }

        public List<MediaManageViewModel> Medias { get; set; }

        public string LanguageName { get; set; }
        public int LanguageId { get; set; }

        public SectionManageModel() { }

        public SectionManageModel(Core.Section entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Image = entity.Image;
            Content = entity.Content;
            ImageFolder = entity.ImageFolder;
            LanguageName = entity.Language.Name;
            LanguageId = entity.Language.Id;
            //if (entity.Order == SectionTypeCollection.Section3)
            //{
                IsMedia = true;
                Medias = new List<MediaManageViewModel>();
                int i = 0;
                if (!string.IsNullOrEmpty(Image))
                {
                    JsonHelper.Deserialize<List<Image>>(Image).ForEach(x => {
                        Medias.Add(new MediaManageViewModel(x)
                        {
                            Position = i++
                        });
                    });
                }
            //}
            Order = entity.Order;
        }

        public static void ToEntity(SectionManageModel model, ref Core.Section entity)
        {
            entity.Title = model.Title;
            entity.Content = model.Content;
            entity.Image = model.Image;
            //if (entity.Order == SectionTypeCollection.Section3)
            //{
            //    if (model.Medias != null)
            //    {
            //        var entityMedias = new List<Image>();
            //        // Insert Media
            //        foreach (var media in model.Medias.Where(x => !x.IsDeleted))
            //        {
            //            var entityMedia = new Image()
            //            {
            //            };
            //            // Parse data from model to entity
            //            MediaManageViewModel.ToEntity(media, ref entityMedia);
            //            // Insert entity into db
            //            entityMedias.Add(entityMedia);
            //        }
            //        entity.Image = JsonHelper.Serialize(entityMedias);
            //    }
            //}
        }
    }

    public class MediaManageViewModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên hình!")]
        [StringLength(200, ErrorMessage = "Không được vượt quá 200 ký tự!")]
        public string Name { get; set; }

        public bool IsDeleted { get; set; }

        public int Position { get; set; }

        public int Order { get; set; }

        [StringLength(200, ErrorMessage = "Không được vượt quá 200 ký tự!")]
        public string AltImage { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập đường dẫn ảnh!")]
        public string ImageDefault { get; set; }

        public Core.Album Album { get; set; }

        public string PropertyName { get; set; }

        public MediaManageViewModel() { }

        public MediaManageViewModel(Image entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            Order = entity.Order;
            AltImage = entity.Alt;
            ImageDefault = entity.Path;
            IsDeleted = false;
        }

        public static void ToEntity(MediaManageViewModel model, ref Image entity)
        {
            entity.Name = model.Name;
            entity.Order = model.Order;
            entity.Alt = model.AltImage;
            entity.Path = model.ImageDefault;
        }

        public MediaManageViewModel(Media entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            Order = entity.Order;
            AltImage = entity.Alt;
            ImageDefault = entity.Path;
            IsDeleted = false;
        }

        public static void ToEntity(MediaManageViewModel model, ref Media entity)
        {
            entity.Name = model.Name;
            entity.Order = model.Order;
            entity.Alt = model.AltImage;
            entity.Path = model.ImageDefault;
        }
    }
}
