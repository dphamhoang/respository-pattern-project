﻿using System;

namespace MainProject.Bussiness.Admin.Models
{
    public class ContactManageViewModel
    {
        public string Name { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }

        public ContactManageViewModel() {  }

        public ContactManageViewModel(Core.Contact entity)
        {
            Name = entity.Name;
            Phone = entity.Phone;
            Email = entity.Email;
            Address = entity.Address;
            Content = entity.Content;
            CreatedDate = entity.CreatedDate;
        }
    }
}
