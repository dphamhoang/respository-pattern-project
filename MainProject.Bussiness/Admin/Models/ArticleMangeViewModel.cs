﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MainProject.Bussiness.Admin.Models
{
    public class ArticleManageViewModel 
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Vui lòng tên bài viết!")]
        [StringLength(200, ErrorMessage = "Không được vượt quá 200 ký tự!")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập đường dẫn cho bài viết!")]
        [StringLength(500, ErrorMessage = "Không được vượt quá 500 ký tự!")]
        public string SeName { get; set; }

        [AllowHtml]
        public string Description { get; set; }

        [AllowHtml]
        public string Content { get; set; }

        public string ImageDefault { get; set; }

        public string ImageFolder { get; set; }

        public bool IsPublished { get; set; }
		
		public bool IsHot { get; set; }

        public DateTime Order { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn danh mục!")]
        public long CategorySelectedValue { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn ngôn ngữ!")]
        public int LanguageSelectedValue { get; set; }
        public List<SelectListItem> Languages { get; set; }

        public long LogHistoryId { get; set; }

        #region SEO
        [StringLength(500, ErrorMessage = "Không được vượt quá 500 ký tự!")]
        public string MetaTitle { get; set; }

        public string MetaImage { get; set; }

        [StringLength(500, ErrorMessage = "Không được vượt quá 500 ký tự!")]
        public string MetaDescription { get; set; }

        [StringLength(500, ErrorMessage = "Không được vượt quá 500 ký tự!")]
        public string MetaKeywords { get; set; }

        //[RegularExpression(@"^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#_]*)?$", ErrorMessage ="Chuỗi Url nhập vào không hợp lệ")]
        public string ExternalUrl { get; set; } 
        #endregion

        public ArticleManageViewModel()
        {
        }

        public ArticleManageViewModel(Core.Article entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Description = entity.Description;
            Content = entity.Content;
            ImageDefault = entity.ImageDefault;
            ImageFolder = entity.ImageFolder;
            ExternalUrl = entity.ExternalUrl;
            IsPublished = entity.IsPublished;
            IsHot = entity.IsHot;
            SeName = entity.SeName;
            Order = entity.Order;
            // SEO
            MetaTitle = entity.MetaTitle;
            MetaImage = entity.MetaImage;
            MetaKeywords = entity.MetaKeywords;
            MetaDescription = entity.MetaDescription;
        }

        public static void ToEntity(ArticleManageViewModel model, ref Core.Article entity)
        {
            entity.Title = model.Title;
            entity.Description = model.Description;
            entity.Content = model.Content;
            entity.ImageDefault = model.ImageDefault;
            entity.ImageFolder = model.ImageFolder;
            entity.ExternalUrl = model.ExternalUrl;
            entity.IsPublished = model.IsPublished;
            entity.SeName = model.SeName;
            entity.IsHot = model.IsHot;
            entity.Order = model.Order;
            // SEO
            entity.MetaTitle = model.MetaTitle;
            entity.MetaImage = model.MetaImage;
            entity.MetaKeywords = model.MetaKeywords;
            entity.MetaDescription = model.MetaDescription;
        }

    }
}
