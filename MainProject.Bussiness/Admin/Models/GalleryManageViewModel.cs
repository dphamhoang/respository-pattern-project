﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MainProject.Bussiness.Admin.Models
{
    public class GalleryManageViewModel
    {
        public long Id { get; set; }

        public DateTime Order { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập đường dẫn ảnh mặc định!")]
        [StringLength(500, ErrorMessage = "Không được vượt quá 500 ký tự!")]
        public string ResourcePath { get; set; }

        [StringLength(500, ErrorMessage = "Không được vượt quá 500 ký tự!")]
        public string ImageFolder { get; set; }

        public bool IsVideo { get; set; }

        public bool IsPublished { get; set; }

        public List<GalleryLanguageTranslation> GalleryLanguageTranslations { get; set; }

        public List<MediaManageViewModel> Medias { get; set; }

        public DateTime CreatedDate { get; set; }

        public long LogHistoryId { get; set; }

        public GalleryManageViewModel() { }

        public GalleryManageViewModel(Core.Album entity)
        {
            Id = entity.Id;
            Order = entity.Order;
            ResourcePath = entity.ResourcePath;
            ImageFolder = entity.ImageFolder;
            IsPublished = entity.IsPublished;
            IsVideo = entity.IsVideo;
            CreatedDate = entity.CreatedDate;
        }

        public static void ToEntity(GalleryManageViewModel model, ref Core.Album entity)
        {
            entity.Order = model.Order;
            entity.ResourcePath = model.ResourcePath;
            entity.ImageFolder = model.ImageFolder;
            entity.IsPublished = model.IsPublished;
            entity.IsVideo = model.IsVideo;
        }
    }

    public class GalleryLanguageTranslation
    {
        public long Id { get; set; }

        public int LanguageId { get; set; }

        public string LanguageName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tiêu đề Album!")]
        [StringLength(500, ErrorMessage = "Không được vượt quá 500 ký tự!")]
        public string Name { get; set; }

        public GalleryLanguageTranslation() { }

        public GalleryLanguageTranslation(Core.Album entity)
        {
            Id = entity.Id;
            LanguageId = entity.Language.Id;
            LanguageName = entity.Language.Name;
            Name = entity.Name;
        }
    }
}
