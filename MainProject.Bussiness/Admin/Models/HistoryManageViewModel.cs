﻿using MainProject.Framework.Models;
using MainProject.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainProject.Bussiness.Admin.Models
{
    public class HistoryManageViewModel
    {
        public string ActionBy { get; set; }

        public List<MainProject.Core.LogHistory> ListHistoryContent { get; set; }

        public PagingModel PagingViewModel { get; set; }

        public string Name { get; set; }
    }
}
