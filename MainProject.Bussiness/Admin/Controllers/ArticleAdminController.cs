﻿using System.Net;
using System.Web.Mvc;
using MainProject.Core.Enums;
using MainProject.Framework.Attributes;
using MainProject.Bussiness.Admin.Services;
using MainProject.Framework.Controllers;
using MainProject.Bussiness.Admin.Models;

namespace MainProject.Bussiness.Admin.Controllers
{
    public class ArticleAdminController : BaseController
    {     
        private readonly ArticleService _service;

        public ArticleAdminController()
        {
            _service = new ArticleService(DbContext);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageNews, Permission = PermissionCollection.View)]
        public ActionResult Index(string text, int languageId = 1, long fa = 0, int page = 1)
            => View(_service.GetIndex(text, languageId, fa, page));

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageNews, Permission = PermissionCollection.Create)]
        public ActionResult Create()
        {
            var model = _service.Create();
            _service.BindSelectListItem(model);
            return View(model);
        }

        [HttpPost]
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageNews, Permission = PermissionCollection.Create)]
        public ActionResult Create(ArticleManageViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData["message"] = _service.Insert(model);
                return RedirectToAction("Index", new { languageId = model.LanguageSelectedValue });         
            }

            _service.BindSelectListItem(model);
            return View(model);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageNews, Permission = PermissionCollection.Edit)]
        public ActionResult Edit(int id)
        {
            var result = _service.Edit(id);
            if (result.Code == HttpStatusCode.BadRequest)
            {
                TempData["message"] = result;
                return RedirectToAction("Index");
            }

            _service.BindSelectListItem(result.Result);
            return View(result.Result);
        }

        [HttpPost]
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageNews, Permission = PermissionCollection.Edit)]
        public ActionResult Edit(ArticleManageViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _service.Update(model);
                if (result.Code == HttpStatusCode.OK)
                {
                    TempData["message"] = result;
                    return RedirectToAction("Index", new { languageId = model.LanguageSelectedValue, fa = model.CategorySelectedValue});
                }
            }

            _service.BindSelectListItem(model);
            return View(model);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageNews, Permission = PermissionCollection.Delete)]
        [HttpPost]
        public ActionResult Delete(long id) => Json(_service.Delete(id));
    }
}
