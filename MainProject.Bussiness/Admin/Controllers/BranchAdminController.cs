﻿using MainProject.Core.Enums;
using MainProject.Framework.Attributes;
using MainProject.Bussiness.Admin.Services;
using MainProject.Framework.Controllers;
using System.Net;
using System.Web.Mvc;
using MainProject.Bussiness.Admin.Models;

namespace MainProject.Bussiness.Admin.Controllers
{
    public class BranchAdminController : BaseController
    {
        private readonly BranchService _service;

        public BranchAdminController()
        {
            _service = new BranchService(DbContext);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.View)]
        public ActionResult Index(int languageId = 1, int page = 1)
            => View(_service.GetIndex(languageId, page));

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.Create)]
        public ActionResult Create(int id = 0)
        {
            var model = _service.Create();
            _service.BindSelectListItem(model);
            return View(model);
        }

        [HttpPost]
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.Create)]
        public ActionResult Create(BranchManageViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData["message"] = _service.Insert(model);
                return RedirectToAction("Index", new { languageId = model.LanguageSelectedValue });
            }
            _service.BindSelectListItem(model);
            return View(model);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.Edit)]
        public ActionResult Edit(long id)
        {
            var model = _service.Edit(id);
            if (model.Code == HttpStatusCode.BadRequest)
            {
                TempData["message"] = model;
                return RedirectToAction("Index");
            }
            return View(model.Result);
        }

        [HttpPost]
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.Edit)]
        public ActionResult Edit(BranchManageViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _service.Update(model);
                if (result.Code == HttpStatusCode.OK)
                {
                    TempData["message"] = result;
                    return RedirectToAction("Index", new { LanguageId = model.LanguageSelectedValue });
                }
            }
            _service.BindSelectListItem(model);
            return View(model);
        }

        //Delete
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.Delete)]
        [HttpPost]
        public ActionResult Delete(int id) => Json(_service.Delete(id));
    }
}