﻿using MainProject.Core.Enums;
using MainProject.Framework.Attributes;
using MainProject.Bussiness.Admin.Models;
using MainProject.Bussiness.Admin.Services;
using MainProject.Framework.Controllers;
using System.Net;
using System.Web.Mvc;

namespace MainProject.Bussiness.Admin.Controllers
{
    public class SectionAdminController : BaseController
    {
        private readonly SectionService _service;

        public SectionAdminController()
        {
            _service = new SectionService(DbContext);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageSections, Permission = PermissionCollection.View)]
        // GET: Admin/SectionAdmin
        public ActionResult Index(int cul = 1) => View(_service.Get(cul));

        public ActionResult CreateMedia(int pos)
           => PartialView("_Media", new MediaManageViewModel() { Position = pos });

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageSections, Permission = PermissionCollection.Edit)]
        public ActionResult Edit(int id)
        {
            var result = _service.Edit(id);
            if (result.Code == HttpStatusCode.BadRequest)
            {
                TempData["message"] = result;
                return RedirectToAction("Index");
            }

            return View(result.Result);
        }

        [HttpPost]
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageSections, Permission = PermissionCollection.Edit)]
        public ActionResult Edit(SectionManageModel model)
        {
            if (ModelState.IsValid)
            {
                // Check whether update category data is successful
                var result = _service.Update(model);
                if (result.Code == HttpStatusCode.OK)
                {
                    TempData["message"] = result;
                    return RedirectToAction("Index", new { LanguageId = model.LanguageId });
                }
            }

            return View(model);
        }
    }
}