﻿using MainProject.Core.Enums;
using MainProject.Framework.Controllers;
using System.Web.Mvc;
using MainProject.Framework.Attributes;
using MainProject.Bussiness.Admin.Services;
using MainProject.Bussiness.Admin.Models;
using System.Net;

namespace MainProject.Bussiness.Admin.Controllers
{
    public class BannerAdminController : BaseController
    {
        private readonly BannerService _service;

        public BannerAdminController()
        {
            _service = new BannerService(DbContext);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBanner, Permission = PermissionCollection.View)]
        public ActionResult Index(int languageId = 1, int page = 1)
            => View(_service.GetIndex(languageId, page));

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBanner, Permission = PermissionCollection.Create)]
        public ActionResult Create()
        {
            var model = _service.Create();
            _service.BindSelectListItem(model);
            return View(model);
        }

        [HttpPost]
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBanner, Permission = PermissionCollection.Create)]
        public ActionResult Create(BannerManageViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _service.Insert(model);
                if (result.Code == HttpStatusCode.OK)
                {
                    TempData["message"] = result;
                    return RedirectToAction("Index");
                }
            }
            _service.BindSelectListItem(model);
            return View(model);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBanner, Permission = PermissionCollection.Edit)]
        public ActionResult Edit(long id)
        {
            var model = _service.Edit(id);
            if (model.Code == HttpStatusCode.BadRequest)
            {
                TempData["message"] = model;
                return RedirectToAction("Index");
            }
            _service.BindSelectListItem(model.Result);
            return View(model.Result);
        }

        [HttpPost]
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBanner, Permission = PermissionCollection.Edit)]
        public ActionResult Edit(BannerManageViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _service.Update(model);
                if (result.Code == HttpStatusCode.OK)
                {
                    TempData["message"] = result;
                    return RedirectToAction("Index", new { LanguageId = model.LanguageSelectedValue });
                }
            }
            _service.BindSelectListItem(model);
            return View(model);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBanner, Permission = PermissionCollection.Delete)]
        [HttpPost]
        public ActionResult Delete(int id) => Json(_service.Delete(id));
    }
}