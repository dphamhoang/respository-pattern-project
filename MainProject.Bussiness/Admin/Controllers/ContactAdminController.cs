﻿using System.Web.Mvc;
using MainProject.Framework.Controllers;
using MainProject.Framework.Attributes;
using MainProject.Core.Enums;
using MainProject.Bussiness.Admin.Services;
using System.Net;

namespace MainProject.Bussiness.Admin.Controllers
{
    public class ContactAdminController : BaseController
    {
        private readonly ContactService _service;

        public ContactAdminController()
        {
            _service = new ContactService(DbContext);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageContact, Permission = PermissionCollection.View)]
        public ActionResult Index(int page = 1) => View(_service.GetIndex(page));

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageContact, Permission = PermissionCollection.View)]
        public ActionResult Detail(long id)
        {
            var model = _service.Detail(id);
            if (model.Code == HttpStatusCode.BadRequest)
            {
                TempData["message"] = model;
                return RedirectToAction("Index");
            }
            return View(model.Result);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageContact, Permission = PermissionCollection.Delete)]
        [HttpPost]
        public ActionResult Delete(long id) => Json(_service.Delete(id));
    }
}
