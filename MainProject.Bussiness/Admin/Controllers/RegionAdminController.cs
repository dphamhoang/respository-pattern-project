﻿using MainProject.Core.Enums;
using MainProject.Framework.Attributes;
using MainProject.Bussiness.Admin.Services;
using MainProject.Framework.Controllers;
using System.Net;
using System.Web.Mvc;
using MainProject.Bussiness.Admin.Models;

namespace MainProject.Bussiness.Admin.Controllers
{
    public class RegionAdminController : BaseController
    {
        private readonly RegionService _service;

        public RegionAdminController()
        {
            _service = new RegionService(DbContext);
        }

        // GET: Admin/Region
        #region City
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.View)]
        public ActionResult CityIndex(int page = 0)
        {
            return View(_service.GetCity(page));
        }

        [HttpPost]
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.Create)]
        public ActionResult CityCreate(CityManageViewModel model)
        {
            TempData["message"] = _service.InsertCity(model).Message;
            return RedirectToAction("CityIndex");
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.Edit)]
        public ActionResult CityEdit(int id)
        {
            var result = _service.EditCity(id);
            if (result.Code == HttpStatusCode.BadRequest)
            {
                TempData["message"] = result.Message;
                return RedirectToAction("CityIndex");
            }

            return View(result.Result);
        }

        [HttpPost]
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.Edit)]
        public ActionResult CityEdit(CityManageViewModel model)
        {
            TempData["message"] = _service.UpdateCity(model).Message;
            return RedirectToAction("CityIndex");
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.Delete)]
        public ActionResult CityDelete(int id)
        {
            TempData["message"] = _service.DeleteCity(id).Message;
            return RedirectToAction("CityIndex");
        } 
        #endregion

        #region District
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.View)]
        public ActionResult DistrictIndex(int city = 0, int page = 0)
        {
            return View(_service.GetDistrict(city, page));
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.Create)]
        public ActionResult DistrictCreate()
        {
            return View(_service.CreateDistrict());
        }

        [HttpPost]
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.Create)]
        public ActionResult DistrictCreate(DisctrictManageViewModel model)
        {
            TempData["message"] = _service.InsertDisctrict(model).Message;
            return RedirectToAction("DistrictIndex");
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.Edit)]
        public ActionResult DistrictEdit(int id)
        {
            var result = _service.EditDisctrict(id);
            if (result.Code == HttpStatusCode.BadRequest)
            {
                TempData["message"] = result.Message;
                return RedirectToAction("DistrictIndex");
            }

            return View(result.Result);
        }

        [HttpPost]
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.Edit)]
        public ActionResult DistrictEdit(DisctrictManageViewModel model)
        {
            TempData["message"] = _service.UpdateDisctrict(model).Message;
            return RedirectToAction("DistrictIndex");
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageBranch, Permission = PermissionCollection.Delete)]
        public ActionResult DistrictDelete(int id)
        {
            TempData["message"] = _service.DeleteCity(id).Message;
            return RedirectToAction("DistrictIndex");
        }
        #endregion

        #region Common Api
        [Route("get-city")]
        public ActionResult GetCity()
            => Json(_service.GetCityApi(), JsonRequestBehavior.AllowGet);

        [Route("get-district")]
        public ActionResult GetDistrict(int cityId, int id = 0)
            => Json(_service.GetDistrictApi(cityId, id), JsonRequestBehavior.AllowGet);

        [Route("get-branch")]
        public ActionResult GetBranch(int cityId, int id = 0, string address = "")
            => Json(_service.GetBranchApi(cityId, id, address), JsonRequestBehavior.AllowGet);
        #endregion
    }
}