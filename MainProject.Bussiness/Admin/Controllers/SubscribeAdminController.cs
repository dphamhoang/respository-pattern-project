﻿using MainProject.Framework.Controllers;
using System.Web.Mvc;
using MainProject.Core.Enums;
using MainProject.Framework.Attributes;
using MainProject.Bussiness.Admin.Services;
using MainProject.Bussiness.Admin.Models;

namespace MainProject.Bussiness.Admin.Controllers
{
    public class SubscribeAdminController : BaseController
    {
        private readonly SubscribeService _service;

        public SubscribeAdminController()
        {
            _service = new SubscribeService(DbContext);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageSubscribe, Permission = PermissionCollection.View)]
        public ActionResult Index(int page = 1)
        {
            return View(_service.GetIndex(page));
        }

        [HttpPost]
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageSubscribe, Permission = PermissionCollection.Edit)]
        public JsonResult EditNote(SubscribeManageModel model)
            => Json(_service.EditNote(ModelState.IsValid, model), JsonRequestBehavior.AllowGet);

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageSubscribe, Permission = PermissionCollection.Delete)]
        public ActionResult Delete(long id) => Json(_service.Delete(id));
    }
}
