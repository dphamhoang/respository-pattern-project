﻿using System.Web.Mvc;
using MainProject.Framework.Controllers;
using MainProject.Bussiness.Admin.Services;
using MainProject.Framework.Attributes;

namespace MainProject.Bussiness.Admin.Controllers
{
    [LogonAuthorize]
    public class AdminCommonController : BaseController
    {
        private readonly CommonService _commonService;

        public AdminCommonController()
        {
            _commonService = new CommonService(DbContext);
        }

        public JsonResult GetCategoryByLanague(int id, long cateId, long exeptedId)
            => Json(new { Data = _commonService.GetCategories(id, cateId, exeptedId) }, JsonRequestBehavior.AllowGet);

        public JsonResult GetMenuByLanguage(int languageId, int selectedValue = 0)
            => Json(new { Data = _commonService.GetMenus(languageId, selectedValue) }, JsonRequestBehavior.AllowGet);

        public JsonResult GetNewsCategoryByLanguage(int languageId, long selectedValue = 0)
            => Json(new { Data = _commonService.GetCategories(languageId, selectedValue, -1) }, JsonRequestBehavior.AllowGet);
    }
}
