﻿using System.Web.Mvc;
using MainProject.Core.Enums;
using MainProject.Framework.Attributes;
using MainProject.Framework.Controllers;
using MainProject.Bussiness.Admin.Services;
using System.Net;
using MainProject.Bussiness.Admin.Models;

namespace MainProject.Bussiness.Admin.Controllers
{
    public class CategoryAdminController : BaseController
    {
        private readonly CategoryService _service;

        public CategoryAdminController()
        {
            _service = new CategoryService(DbContext);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageCategory, Permission = PermissionCollection.View)]
        public ActionResult Index(int languageId = 1, int page = 1) => View(_service.GetIndex(languageId, page));

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageCategory, Permission = PermissionCollection.Create)]
        public ActionResult Create() 
        {
            var model = _service.Create();
            _service.BindSelectListItem(model);
            return View(model);
        }

        [HttpPost]
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageCategory, Permission = PermissionCollection.Create)]
        public ActionResult Create(CategoryManageViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData["message"] = _service.Insert(model);
                return RedirectToAction("Index", new { languageId = model.LanguageSelectedValue });
            }

            _service.BindSelectListItem(model);
            return View(model);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageCategory, Permission = PermissionCollection.Edit)]
        public ActionResult Edit(long id)
        {
            var model = _service.Edit(id);
            if (model.Code == HttpStatusCode.BadRequest)
            {
                TempData["message"] = model;
                return RedirectToAction("Index");
            }

            _service.BindSelectListItem(model.Result);
            return View(model.Result);
        }

        [HttpPost]
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageCategory, Permission = PermissionCollection.Edit)]
        public ActionResult Edit(CategoryManageViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Check whether update category data is successful
                var result = _service.Update(model);
                if (result.Code == HttpStatusCode.OK)
                {
                    TempData["message"] = result;
                    return RedirectToAction("Index", new { LanguageId = model.LanguageSelectedValue });
                }
            }

            _service.BindSelectListItem(model);
            return View(model);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageCategory, Permission = PermissionCollection.Delete)]
        [HttpPost]
        public ActionResult Delete(int id) => Json(_service.Delete(id));
    }
}