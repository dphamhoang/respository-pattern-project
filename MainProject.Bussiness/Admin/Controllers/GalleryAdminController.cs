﻿using MainProject.Core.Enums;
using MainProject.Framework.Attributes;
using MainProject.Bussiness.Admin.Services;
using MainProject.Framework.Controllers;
using System.Net;
using System.Web.Mvc;
using MainProject.Bussiness.Admin.Models;

namespace MainProject.Bussiness.Admin.Controllers
{
    public class GalleryAdminController : BaseController
    {
        private readonly GalleryService _service;

        public GalleryAdminController()
        {
            _service = new GalleryService(DbContext);
        }

        // GET: Admin/Gallery

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageGallery, Permission = PermissionCollection.View)]
        public ActionResult Index(bool isVideo = false, int page = 1) => View(_service.GetIndex(isVideo, page));

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageGallery, Permission = PermissionCollection.Create)]
        public ActionResult Create(bool isVideo = false)
        {
            var model = _service.Create(isVideo);
            _service.BindSelectListItem(model);
            return View(model);
        }

        public ActionResult CreateMedia(int pos)
            => PartialView("_Media", new MediaManageViewModel() { Position = pos });

        [HttpPost]
        public ActionResult Create(GalleryManageViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData["message"] = _service.Insert(model);
                return RedirectToAction("Index", new { isVideo = model.IsVideo });
            }
            // In case some error's occurred, need to rebind model
            _service.BindSelectListItem(model);
            return View(model);
        }

        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageGallery, Permission = PermissionCollection.Edit)]
        public ActionResult Edit(long id, bool isVideo = false)
        {
            var model = _service.Edit(id);
            if (model.Code == HttpStatusCode.BadRequest)
            {
                TempData["message"] = model;
                return RedirectToAction("Index", new { isVideo = isVideo });
            }
            _service.BindSelectListItem(model.Result);
            return View(model.Result);
        }

        [HttpPost]
        public ActionResult Edit(GalleryManageViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _service.Update(model);
                if (result.Code == HttpStatusCode.OK)
                {
                    TempData["message"] = result;
                    return RedirectToAction("Index", new { isVideo = model.IsVideo });
                }
            }
            // In case some error's occurred, need to rebind model
            _service.BindSelectListItem(model);
            return View(model);
        }

        //Delete
        [AuthorizeUserAttribute(Feature = EntityManageTypeCollection.ManageGallery, Permission = PermissionCollection.Delete)]
        [HttpPost]
        public ActionResult Delete(int id) => Json(_service.Delete(id));
    }
}