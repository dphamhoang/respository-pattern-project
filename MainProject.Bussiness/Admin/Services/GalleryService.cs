﻿using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Infrastructure.Helpers;
using MainProject.Data.Repositories;
using MainProject.Framework.Constant;
using MainProject.Framework.Helpers;
using MainProject.Infrastructure.Models;
using MainProject.Bussiness.Admin.Models;
using MainProject.Framework.Services;
using MainProject.Framework.Models;
using System;
using System.Linq;
using System.Net;

namespace MainProject.Bussiness.Admin.Services
{
    public class GalleryService
    {
        private readonly AlbumRepository _albumRepository;
        private readonly MediaRepository _mediaRepository;
        private readonly LanguageRepository _languageRepository;
        private readonly LogHistoryService _logHistoryService;
        private readonly int _itemPerPage = 15;

        public GalleryService(MainDbContext dbContext)
        {
            _albumRepository = new AlbumRepository(dbContext);
            _mediaRepository = new MediaRepository(dbContext);
            _languageRepository = new LanguageRepository(dbContext);
            _logHistoryService = new LogHistoryService(dbContext, EntityTypeCollection.Gallery);
        }

        public IndexViewModel<Core.Album> GetIndex(bool isVideo, int page)
        {
            if (page < 1) page = 1;
            // Get entity by language
            var query = _albumRepository.Find(x => x.Language.Id == 1 && x.IsVideo == isVideo);

            return new IndexViewModel<Core.Album>()
            {
                ListItems = query.OrderBy(d => d.Id).Skip((page - 1) * _itemPerPage).Take(_itemPerPage).ToList(),
                FilterViewModel = new FilterViewModel()
                {
                    BaseUrl = "/Admin/GalleryAdmin/Index?isVideo=" + isVideo,
                    Title = isVideo.ToString()
                },
                PagingViewModel = new PagingModel(query.Count(), _itemPerPage, page, "href='/Admin/GalleryAdmin/Index?isVideo=" + isVideo + "&page={0}'")
            };
        }

        /// <summary>
        /// Initialize data for create view
        /// </summary>
        /// <returns></returns> 
        public GalleryManageViewModel Create(bool isVideo)
        {
            var imageFolder = FolderHelper.Generate(FolderConstant.Gallery);

            return new GalleryManageViewModel
            {
                IsVideo = isVideo,
                ImageFolder = imageFolder,
                LogHistoryId = _logHistoryService.Create(new LogHistoryModel()
                {
                    ActionType = ActionTypeCollection.Temp,
                    Comment = imageFolder
                }),
            };
        }

        /// <summary>
        /// Insert data to database
        /// </summary>
        /// <param name="model"></param>
        public BaseResponseModel Insert(GalleryManageViewModel model)
        {
            var date = DateTime.Now;
            foreach (var item in model.GalleryLanguageTranslations)
            {
                var entity = new Core.Album
                {
                    Name = item.Name,
                    Language = _languageRepository.FindId(item.LanguageId),
                    CreatedDate = date
                };
                // Parse data from model to entity
                GalleryManageViewModel.ToEntity(model, ref entity);
                // Insert entity into db
                _albumRepository.Insert(entity);
                // Save media with vietnamese
                if (entity.Language.Id == 1 && model.Medias != null)
                {
                    // Check entity is album images
                    if (!entity.IsVideo)
                    {
                        // Insert Media
                        foreach (var media in model.Medias.Where(x => !x.IsDeleted))
                        {
                            var entityMedia = new Core.Media()
                            {
                                Album = entity
                            };
                            // Parse data from model to entity
                            MediaManageViewModel.ToEntity(media, ref entityMedia);
                            // Insert entity into db
                            _mediaRepository.Insert(entityMedia);
                        }
                    }

                    // Update change logHistory from temp to create
                    _logHistoryService.Update(model.LogHistoryId, entity.Id);
                }
            }

            return new BaseResponseModel
            {
                Code = HttpStatusCode.OK,
                Message = "Thêm mới thành công!"
            };
        }

        /// <summary>
        /// Get album for Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BaseResponseModel<GalleryManageViewModel> Edit(long id)
        {
            // Get entity bind data to model
            var entity = _albumRepository.FindUnique(x => x.Id == id);
            // Check entity is exist
            if (entity == null)
            {
                return new BaseResponseModel<GalleryManageViewModel>
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Không tìm thấy album cần sửa"
                };
            }

            return new BaseResponseModel<GalleryManageViewModel>
            {
                Code = HttpStatusCode.OK,
                Result = new GalleryManageViewModel(entity)
            };
        }

        /// <summary>
        /// Update data album to databse
        /// </summary>
        /// <param name="model"></param>
        public BaseResponseModel Update(GalleryManageViewModel model)
        {
            // Get entity to update data
            foreach (var item in model.GalleryLanguageTranslations)
            {
                var entity = _albumRepository.FindUnique(d => d.Id == item.Id);
                if (entity == null)
                {
                    return new BaseResponseModel()
                    {
                        Code = HttpStatusCode.BadRequest,
                        Message = "Album không tồn tại!"
                    };
                }

                // Check entity is change form not video to video for deleting medias
                bool isVideo = entity.IsVideo == model.IsVideo;
                // Parse data from model to entity
                GalleryManageViewModel.ToEntity(model, ref entity);
                // Parse data language
                entity.Name = item.Name;
                // Save changes data album to entity
                _albumRepository.SaveChanges();
                // Check entity is album images
                if (isVideo)
                {
                    if (model.Medias != null)
                    {
                        // Insert Media
                        foreach (var media in model.Medias.Where(x => !x.IsDeleted))
                        {
                            // Get media does exist in db
                            var entityMedia = _mediaRepository.FindUnique(x => x.Id == media.Id);
                            // Create media
                            if (entityMedia == null)
                            {
                                entityMedia = new Core.Media()
                                {
                                    Album = entity
                                };
                                // Parse data from model to entity
                                MediaManageViewModel.ToEntity(media, ref entityMedia);
                                // Insert entity into db
                                _mediaRepository.Insert(entityMedia);
                            }
                            else // Edit media
                            {
                                // Parse data from model to entity
                                MediaManageViewModel.ToEntity(media, ref entityMedia);
                                // Save change
                                _mediaRepository.SaveChanges();
                            }
                        }

                        // Delete media is removed
                        var listId = model.Medias.Where(x => x.IsDeleted).Select(y => y.Id);
                        _mediaRepository.DeleteByCriteria(x => x.Album.Id == entity.Id && listId.Contains(x.Id));
                    }
                    else // Delete all medias is removed
                    {
                        _mediaRepository.DeleteByCriteria(x => x.Album.Id == entity.Id);
                    }
                }
                else // Delete all medias by old isVideo entity
                {
                    _mediaRepository.DeleteByCriteria(x => x.Album.Id == entity.Id);
                }

            }
            // save logHistory 
            _logHistoryService.Create(new LogHistoryModel {
                EntityId = model.GalleryLanguageTranslations.FirstOrDefault().Id,
                ActionType = ActionTypeCollection.Edit
            });

            return new BaseResponseModel()
            {
                Code = HttpStatusCode.OK,
                Message = "Cập nhật album thành công!"
            };
        }

        /// <summary>
        /// Delete data album form database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BaseResponseModel Delete(long id)
        {
            // Get entity to delete
            var entity = _albumRepository.FindUnique(x => x.Id == id);
            // Check album is exist
            if (entity == null)
            {
                return new BaseResponseModel()
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Không tìm thấy đối tượng cần xóa"
                };
            }
            // Delete media
            _mediaRepository.DeleteByCriteria(x => x.Album.Id == id);
            // Delete album
            _albumRepository.Delete(entity.CreatedDate);
            // Save history
            _logHistoryService.Insert(new LogHistoryModel() { EntityId = entity.Id, ActionType = ActionTypeCollection.Delete });
            // Delete image folder
            FolderHelper.Delete(entity.ImageFolder);

            return new BaseResponseModel()
            {
                Code = HttpStatusCode.OK,
                Message = "Xóa thành công"
            };
        }

        public void BindSelectListItem(GalleryManageViewModel model)
        {
            if (model.Id != 0)
            {
                model.GalleryLanguageTranslations =
                        _albumRepository.Find(model.CreatedDate).OrderBy(x => x.Language.Id)
                               .ToList().Select(x => new GalleryLanguageTranslation(x)).ToList();
                model.Medias = _mediaRepository.Find(x => x.Album.Id == model.Id).ToList()
                                         .Select((x, posIndex) => new MediaManageViewModel(x) { Position = posIndex }).ToList();
            }
            else
            {
                model.GalleryLanguageTranslations = _languageRepository.FindAll().Select(
                                                        x => new GalleryLanguageTranslation()
                                                        {
                                                            LanguageId = x.Id,
                                                            LanguageName = x.Name
                                                        }).ToList();
            }
        }
    }
}
