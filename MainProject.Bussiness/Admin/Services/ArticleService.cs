﻿using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Data.Repositories;
using MainProject.Framework.Constant;
using MainProject.Framework.Helpers;
using MainProject.Infrastructure.Models;
using MainProject.Bussiness.Admin.Models;
using MainProject.Bussiness.Helpers;
using MainProject.Framework.Services;
using MainProject.Framework.Models;
using System;
using System.Linq;
using System.Net;

namespace MainProject.Bussiness.Admin.Services
{
    public class ArticleService
    {
        #region Fields
        private readonly ArticleRepository _articleRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly LanguageRepository _languageRepository;
        private readonly LogHistoryService _logHistoryService;
        private readonly int _itemPerPage = 10; 
        #endregion

        public ArticleService(MainDbContext dbContext)
        {
            _articleRepository = new ArticleRepository(dbContext);
            _categoryRepository = new CategoryRepository(dbContext);
            _languageRepository = new LanguageRepository(dbContext);
            _logHistoryService = new LogHistoryService(dbContext, EntityTypeCollection.Articles);
        }

        /// <summary>
        /// Get data filter for Index View
        /// </summary>
        /// <param name="text">Title of article</param>
        /// <param name="cul">Language of article</param>
        /// <param name="fa">Category of article</param>
        /// <param name="page">Current page</param>
        /// <returns></returns>
        /// <summary>
        /// Get data filter for Index View
        /// </summary>
        /// <param name="text">Title of article</param>
        /// <param name="cul">Language of article</param>
        /// <param name="fa">Category of article</param>
        /// <param name="page">Current page</param>
        /// <returns></returns>
        public IndexViewModel<Core.Article> GetIndex(string text, int cul, long fa = 0, int page = 1)
        {
            if (page < 1) page = 1;
            var sql = _articleRepository.Find(x => x.Language.Id == cul);

            // Filter by category.
            if (fa != 0)
            {
                sql = sql.Where(x => x.Category.Id == fa);
            }
            // Filter by title
            if (!string.IsNullOrEmpty(text))
            {
                sql = sql.Where(x => x.Title.Contains(text));
            }

            // Bind model for view
            return new IndexViewModel<Core.Article>()
            {
                ListItems = sql.OrderByDescending(x => x.Id).Skip((page - 1) * _itemPerPage).Take(_itemPerPage).ToList(),
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        Languages = LanguageHelper.BindDropdown(_languageRepository.FindAll().ToList(), 0),
                        LanguageSelectedValue = cul
                    },
                    FatherSelectModel = new FatherSelectModel()
                    {
                        FatherSelectedValue = fa
                    },
                    BaseUrl = "/Admin/ArticleAdmin/Index?"
                },
                PagingViewModel = new PagingModel(sql.Count(), _itemPerPage, page, "href='/Admin/ArticleAdmin/Index?cul=" + cul + "&fa=" + fa + "&page={0}'")
            };
        }

        /// <summary>
        /// Prepare data for Create View
        /// </summary>
        /// <returns></returns>
        public ArticleManageViewModel Create()   
        {
            var imageFolder = FolderHelper.Generate(FolderConstant.Article);

            return new ArticleManageViewModel
            {
                LogHistoryId = _logHistoryService.Create(new LogHistoryModel()
                {
                    ActionType = ActionTypeCollection.Temp,
                    Comment = imageFolder
                }),
                ImageFolder = imageFolder,
            };
        }

        /// <summary>
        /// Insert data to db
        /// </summary>
        /// <param name="model"></param>
        public BaseResponseModel Insert(ArticleManageViewModel model)
        {
            // Initialize Entity
            var entity = new Core.Article()
            {
                Category = _categoryRepository.FindId(model.CategorySelectedValue),
                CreateDate = DateTime.Now,
                Language = _languageRepository.FindId(model.LanguageSelectedValue),
                UpdateDate = DateTime.Now,
            };
            // Parse data from model to entity
            ArticleManageViewModel.ToEntity(model, ref entity);
            // Insert to Article Id for generating url
            _articleRepository.Insert(entity);
            // Generating url and SeName
            entity.SeName = SeoHelper.ValidateSeNameAndSubmit(EntityTypeCollection.Articles, entity.Id, model.SeName, entity.Title, entity.ExternalUrl, entity.Language.Id);
            // Update save SeName
            _articleRepository.SaveChanges();
            // Update change logHistory from temp to create
            _logHistoryService.Update(model.LogHistoryId, entity.Id);

            return new BaseResponseModel
            {
                Code = HttpStatusCode.OK,
                Message = "Thêm mới bài viết thành công!"
            };
        }

        /// <summary>
        /// Get artice for Article Edit view
        /// </summary>
        /// <param name="id">Article id</param>
        /// <returns></returns>
        public BaseResponseModel<ArticleManageViewModel> Edit(int id)
        {
            // Get article for binding data to model
            var entity = _articleRepository.FindUnique(x => x.Id == id);
            // Check article is exist
            if (entity == null)
            {
                return new BaseResponseModel<ArticleManageViewModel>
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Bài viết không tồn tại!"
                };
            }

            return new BaseResponseModel<ArticleManageViewModel>
            {
                Code = HttpStatusCode.OK,
                Result = new ArticleManageViewModel(entity)
                {
                    LanguageSelectedValue = entity.Language.Id,
                    CategorySelectedValue = entity.Category.Id
                }
            };
        }

        /// <summary>
        /// Update article into db
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public BaseResponseModel Update(ArticleManageViewModel model)
        {
            // Get article to update data
            var entity = _articleRepository.FindUnique(x => x.Id == model.Id);
            // Check article is exist
            if (entity == null)
            {
                return new BaseResponseModel()
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Không tìm thấy bài viết!"
                };
            }
            // Bind data from model to article
            ArticleManageViewModel.ToEntity(model, ref entity);
            // Bind category and url
            entity.Language = _languageRepository.FindId(model.LanguageSelectedValue);
            entity.Category = _categoryRepository.FindId(model.CategorySelectedValue);
            _articleRepository.SaveChanges();
            entity.SeName = SeoHelper.ValidateSeNameAndSubmit(EntityTypeCollection.Articles, entity.Id, model.SeName, entity.Title, entity.ExternalUrl, entity.Language.Id);
            _articleRepository.SaveChanges();
            // Save log history
            _logHistoryService.Create(new LogHistoryModel() { EntityId = entity.Id, ActionType = ActionTypeCollection.Edit });

            return new BaseResponseModel()
            {
                Code = HttpStatusCode.OK,
                Message = string.Format("Cập nhật bài viết thành công!")
            };
        }

        /// <summary>
        /// Delete article in db
        /// </summary>
        /// <param name="id">Article id</param>
        /// <returns></returns>
        public BaseResponseModel Delete(long id)
        {
            var entity = _articleRepository.FindUnique(x => x.Id == id);
            if (entity == null)
            {
                return new BaseResponseModel
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Không tìm thấy bài viết!"
                };
            }

            // Delete article url
            SeoHelper.DeleteUrlRecord(EntityTypeCollection.Articles, entity.Id);
            // Delete entity
            _articleRepository.Delete(entity);

            // Save log History
            _logHistoryService.Insert(new LogHistoryModel() { EntityId = entity.Id, ActionType = ActionTypeCollection.Delete });

            // Delete folder
            FolderHelper.Delete(entity.ImageFolder);
            return new BaseResponseModel {
                Code = HttpStatusCode.OK,
                Message = "Đã xóa bài viết thành công!",
            };
        }

        /// <summary>
        /// Bind data to drop down in html
        /// </summary>
        /// <param name="model"></param>
        public void BindSelectListItem(ArticleManageViewModel model)
        {
            model.Languages = LanguageHelper.BindDropdown(_languageRepository.FindAll().ToList(), model.LanguageSelectedValue);
        }
    }
}
