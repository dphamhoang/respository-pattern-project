﻿using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Data.Repositories;
using MainProject.Infrastructure.Models;
using MainProject.Bussiness.Admin.Models;
using MainProject.Framework.Services;
using MainProject.Framework.Models;
using System.Linq;
using System.Net;

namespace MainProject.Bussiness.Admin.Services
{
    public class ContactService
    {
        private readonly ContactRepository _contactRepository;
        private readonly LogHistoryService _logHistoryService;
        private readonly int _itemPerPage = 20;

        public ContactService(MainDbContext dbContext)
        {
            _contactRepository = new ContactRepository(dbContext);
            _logHistoryService = new LogHistoryService(dbContext, EntityTypeCollection.Contact);
        }

        /// <summary>
        /// Get end filter contact
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IndexViewModel<Core.Contact> GetIndex(int page = 1)
        {
            if (page < 1) page = 1;
            var query = _contactRepository.FindAll();

            return new IndexViewModel<Core.Contact>()
            {
                ListItems = query.OrderByDescending(x => x.Id).Skip((page - 1) * _itemPerPage).Take(_itemPerPage).ToList(),
                PagingViewModel = new PagingModel(query.Count(), _itemPerPage, page, "href='/Admin/ContactAdmin/Index?page={0}'")
            };
        }

        /// <summary>
        /// View detail contact
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BaseResponseModel<ContactManageViewModel> Detail(long id)
        {
            var entity = _contactRepository.FindUnique(x => x.Id == id);
            if (entity == null)
            {
                return new BaseResponseModel<ContactManageViewModel>
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = string.Format("Không tìm thấy đối tượng")
                };
            }

            return new BaseResponseModel<ContactManageViewModel>
            {
                Code = HttpStatusCode.OK,
                Result = new ContactManageViewModel(entity)
            };
        }

        /// <summary>
        /// Delete data contact
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BaseResponseModel Delete(long id)
        {
            // Get banner for checking exist and deleting
            var entity = _contactRepository.FindUnique(x => x.Id == id);
            if (entity == null)
            {
                return new BaseResponseModel()
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Không tìm thấy đối tượng cần xóa!"
                };
            }
            // Delete banner
            _contactRepository.Delete(entity);
            // Save history
            _logHistoryService.Insert(new LogHistoryModel() { EntityId = entity.Id, ActionType = ActionTypeCollection.Delete });
            return new BaseResponseModel()
            {
                Code = HttpStatusCode.OK,
                Message = "Xóa liên hệ công!"
            };
        }
    }
}
