﻿using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Data.Repositories;
using MainProject.Infrastructure.Models;
using MainProject.Bussiness.Admin.Models;
using MainProject.Framework.Services;
using MainProject.Framework.Models;
using System.Linq;
using System.Net;

namespace MainProject.Bussiness.Admin.Services
{
    public class SubscribeService
    {
        private readonly SubscribeRepository _subscribeRepository;
        private readonly LogHistoryService _logHistoryService;

        private readonly int pageitem = 10;

        public SubscribeService(MainDbContext dbContext)
        {
            _subscribeRepository = new SubscribeRepository(dbContext);
            _logHistoryService = new LogHistoryService(dbContext, EntityTypeCollection.Subscribe);
        }

        /// <summary>
        /// Get index 
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public IndexViewModel<Core.Subscribe> GetIndex(int page = 1)
        {
            var sql = _subscribeRepository.FindAll();
            var count = sql.Count();
            var subscribes = sql.OrderByDescending(p => p.Id).Skip((page - 1) * pageitem).Take(pageitem).ToList();
            return new IndexViewModel<Core.Subscribe>()
            {
                ListItems = subscribes,
                PagingViewModel = new PagingModel(count, pageitem, page, "href='/Admin/SubscribeAdmin/Index?page={0}'")

            };
        }

        /// <summary>
        /// delete subsribe
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BaseResponseModel Delete(long id)
        {
            // Get subscribe for checking exist and deleting
            var entity = _subscribeRepository.FindUnique(x => x.Id == id);
            if (entity == null)
            {
                return new BaseResponseModel()
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Không tìm thấy đối tượng cần xóa"
                };
            }
            // Delete subscribe
            _subscribeRepository.Delete(entity);
            // Save history
            _logHistoryService.Insert(new LogHistoryModel() { EntityId = entity.Id, ActionType = ActionTypeCollection.Delete });

            return new BaseResponseModel()
            {
                Code = HttpStatusCode.OK,
                Message = "Xóa thành công"
            };
        }

        /// <summary>
        /// Get id for edit note
        /// </summary>
        /// <param name="note"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public BaseResponseModel EditNote(bool modelState, SubscribeManageModel model)
        {
            if (modelState)
            {
                var entity = _subscribeRepository.FindUnique(x => x.Id == model.Id);
                if (entity == null)
                {
                    return new BaseResponseModel
                    {
                        Code = HttpStatusCode.BadRequest,
                        Message = "Email này không tồn tại!"
                    };
                }

                entity.Note = model.Note;
                _subscribeRepository.SaveChanges();
                // save logHistory 
                _logHistoryService.Create(new LogHistoryModel { EntityId = entity.Id, ActionType = ActionTypeCollection.Edit });

                return new BaseResponseModel
                {
                    Code = HttpStatusCode.OK,
                    Message = "Cập nhật lưu ý Email thành công!"
                };
            }

            return new BaseResponseModel
            {
                Code = HttpStatusCode.BadRequest,
                Message = "Ghi chú không được vượt quá 500 ký tự!"
            };
        }

    }
}
