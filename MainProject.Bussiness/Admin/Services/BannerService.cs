﻿using MainProject.Core.Enums;
using MainProject.Data.Repositories;
using MainProject.Infrastructure.Models;
using System.Linq;
using MainProject.Data;
using MainProject.Bussiness.Admin.Models;
using MainProject.Bussiness.Helpers;
using MainProject.Framework.Constant;
using MainProject.Framework.Models;
using MainProject.Framework.Services;
using System.Net;

namespace MainProject.Bussiness.Admin.Services
{
    public class BannerService
    {
        private readonly BannerRepository _bannerRepository;
        private readonly LanguageRepository _languageRepository;
        private readonly LogHistoryService _logHistoryService;
        private readonly int _itemPerPage = 15;

        public BannerService(MainDbContext dbContext)
        {
            _bannerRepository = new BannerRepository(dbContext);
            _languageRepository = new LanguageRepository(dbContext);
            _logHistoryService = new LogHistoryService(dbContext, EntityTypeCollection.Banner);
        }

        /// <summary>
        /// Filter banners
        /// </summary>
        /// <param name="languageId"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public IndexViewModel<MainProject.Core.Banner> GetIndex(int languageId, int page)
        {
            if (page < 1) page = 1;
            // Get banners by language
            var sql = _bannerRepository.Find(x => x.Language.Id == languageId);

            return new IndexViewModel<MainProject.Core.Banner>()
            {
                ListItems = sql.OrderBy(d => d.Id).Skip((page - 1) * _itemPerPage).Take(_itemPerPage).ToList(),
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        Languages = LanguageHelper.BindDropdown(_languageRepository.FindAll().ToList(), languageId),
                        LanguageSelectedValue = languageId
                    },
                    BaseUrl = "/Admin/BannerAdmin/Index?"
                },
                PagingViewModel = new PagingModel(sql.Count(), _itemPerPage, page, "href='/Admin/BannerAdmin/Index?languageId=" + languageId + "&page={0}'")
            };
        }

        /// <summary>
        /// Initialize data for create view
        /// </summary>
        /// <returns></returns> 
        public BannerManageViewModel Create()
        {
            return new BannerManageViewModel
            {
                IsPublished = true
            };
        }

        /// <summary>
        /// Insert data to database
        /// </summary>
        /// <param name="model"></param>
        public BaseResponseModel Insert(BannerManageViewModel model)
        {
            var entity = new Core.Banner
            {
                Language = _languageRepository.FindId(model.LanguageSelectedValue)
            };
            // Parse data from model to entity
            BannerManageViewModel.ToEntity(model, ref entity);
            // Insert to banner Id for generating url
            _bannerRepository.Insert(entity);
            // save logHistory 
            _logHistoryService.Create(new LogHistoryModel { EntityId = entity.Id, ActionType = ActionTypeCollection.Create });

            return new BaseResponseModel
            {
                Code = HttpStatusCode.OK,
                Message = "Thêm mới banner thành công!"
            };
        }

        /// <summary>
        /// Get banner for Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BaseResponseModel<BannerManageViewModel> Edit(long id)
        {
            // Get entity bind data to model
            var entity = _bannerRepository.FindUnique(x => x.Id == id);
            // Check entity is exist
            if (entity == null)
            {
                return new BaseResponseModel<BannerManageViewModel>
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Không tìm thấy banner cần sửa!"
                };
            }

            return new BaseResponseModel<BannerManageViewModel>
            {
                Code = HttpStatusCode.OK,
                Result = new BannerManageViewModel(entity)
            };
        }

        /// <summary>
        /// Update data banner to databse
        /// </summary>
        /// <param name="model"></param>
        public BaseResponseModel Update(BannerManageViewModel model)
        {
            // Get entity to update data
            var entity = _bannerRepository.FindUnique(d => d.Id == model.Id);
            if (entity == null)
                return new BaseResponseModel()
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Banner không tồn tại!"
                };
            // Parse data from model to entity
            BannerManageViewModel.ToEntity(model, ref entity);
            entity.Language = _languageRepository.FindId(model.LanguageSelectedValue);
            // Save changes data banner to entity
            _bannerRepository.SaveChanges();
            // save logHistory 
            _logHistoryService.Create(new LogHistoryModel { EntityId = entity.Id, ActionType = ActionTypeCollection.Edit });

            return new BaseResponseModel()
            {
                Code = HttpStatusCode.OK,
                Message = "Cập nhật banner thành công!"
            };
        }

        /// <summary>
        /// Delete data banner form database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BaseResponseModel Delete(long id)
        {
            // Get entity to delete
            var entity = _bannerRepository.FindById(id);
            // Check banner is exist
            if (entity == null)
            {
                return new BaseResponseModel()
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Không tìm thấy đối tượng cần xóa!"
                };
            }
            // Delete banner
            _bannerRepository.Delete(entity);
            // Save history
            _logHistoryService.Insert(new LogHistoryModel() { EntityId = entity.Id, ActionType = ActionTypeCollection.Delete });

            return new BaseResponseModel()
            {
                Code = HttpStatusCode.OK,
                Message = "Xóa thành công!"
            };
        }

        /// <summary>
        /// Bind select listItem
        /// </summary>
        /// <param name="model"></param>
        public void BindSelectListItem(BannerManageViewModel model)
        {
            model.ImageFolder = FolderConstant.Banner;
            model.Languages = LanguageHelper.BindDropdown(_languageRepository.FindAll().ToList(), model.LanguageSelectedValue);
        }
    }
}
