﻿using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Data.Repositories;
using MainProject.Framework.Helpers;
using MainProject.Infrastructure.Models;
using MainProject.Bussiness.Helpers;
using MainProject.Bussiness.Admin.Models;
using MainProject.Framework.Services;
using MainProject.Framework.Models;
using System.Linq;
using System;
using MainProject.Framework.Constant;
using System.Net;

namespace MainProject.Bussiness.Admin.Services
{
    public class CategoryService
    {
        private readonly CategoryRepository _categoryRepository;
        private readonly LanguageRepository _languageRepository;
        private readonly LogHistoryService _logHistoryService;
        private readonly int _itemPerPage = 10;

        public CategoryService(MainDbContext dbContext)
        {
            _categoryRepository = new CategoryRepository(dbContext);
            _languageRepository = new LanguageRepository(dbContext);
            _logHistoryService = new LogHistoryService(dbContext, EntityTypeCollection.Categories);
        }

        /// <summary>
        /// Filter categories
        /// </summary>
        /// <param name="languageId">Language of category</param>
        /// <param name="page">Current page</param>
        /// <returns></returns>
        public IndexViewModel<Core.Category> GetIndex(int languageId, int page)
        {
            if (page < 1) page = 1;
            var query = _categoryRepository.Find(x => !x.IsSystem && x.Language.Id == languageId);

            return new IndexViewModel<Core.Category>()
            {
                ListItems = query.OrderByDescending(x => x.Id).Skip((page - 1) * _itemPerPage).Take(_itemPerPage).ToList(),
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        Languages = LanguageHelper.BindDropdown(_languageRepository.FindAll().ToList(), languageId),
                        LanguageSelectedValue = languageId
                    },
                },
                PagingViewModel = new PagingModel(query.Count(), _itemPerPage, page, "href='/Admin/CategoryAdmin/Index?languageId=" + languageId + "&page={0}'")
            };
        }

        /// <summary>
        /// Initialize data for Create View
        /// </summary>
        /// <returns></returns>
        public CategoryManageViewModel Create()
        {
            var imageFolder = FolderHelper.Generate(FolderConstant.Category);

            return new CategoryManageViewModel
            {
                ImageFolder = imageFolder,
                // Save logHistory in case User cancel create, this use for deleting unused folder
                LogHistoryId = _logHistoryService.Create(new LogHistoryModel(){
                    ActionType = ActionTypeCollection.Temp,
                    Comment = imageFolder,
                })
            };
        }

        /// <summary>
        /// Insert data into db
        /// </summary>
        /// <param name="model"></param>
        public BaseResponseModel Insert(CategoryManageViewModel model)
        {
            // Initialize Entity
            var entity = new Core.Category
            {
                Language = _languageRepository.FindId(model.LanguageSelectedValue),
                Parent = _categoryRepository.FindId(model.ParentSelectedValue),
                PrivateArea = model.PrivateArea,
                EntityType = EntityTypeCollection.Categories,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
            };
            // Parse data from model to entity
            CategoryManageViewModel.ToEntity(model, ref entity);
            // Insert to category Id for generating url
            _categoryRepository.Insert(entity);
            // Generating url and SeName
            entity.SeName = SeoHelper.ValidateSeNameAndSubmit(EntityTypeCollection.Categories, entity.Id, entity.SeName, entity.Title, entity.ExternalUrl, entity.Language.Id);
            // Update save SeName
            _categoryRepository.SaveChanges();
            // Update change logHistory from temp to create
            _logHistoryService.Update(model.LogHistoryId, entity.Id);

            return new BaseResponseModel
            {
                Code = HttpStatusCode.OK,
                Message = "Thêm mới danh mục thành công!"
            };
        }

        /// <summary>
        /// Get category for Category Edit View
        /// </summary>
        /// <param name="id">Category id</param>
        /// <returns></returns>
        public BaseResponseModel<CategoryManageViewModel> Edit(long id)
        {
            var entity = _categoryRepository.FindId(id);
            if (entity == null)
            {
                return new BaseResponseModel<CategoryManageViewModel>{
                    Code = HttpStatusCode.BadRequest,
                    Message = "Không tìm thấy danh mục!"
                };
            }

            return new BaseResponseModel<CategoryManageViewModel>
            {
                Code = HttpStatusCode.OK,
                Result = new CategoryManageViewModel(entity)
            };
        }

        /// <summary>
        /// Update category into db
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public BaseResponseModel Update(CategoryManageViewModel model)
        {
            // Get edited category for changing data
            var entity = _categoryRepository.FindId(model.Id);
            if (entity == null)
            {
                return new BaseResponseModel
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Không tìm thấy danh mục!"
                };
            }
            // Bind data want to edit from model to entity
            CategoryManageViewModel.ToEntity(model, ref entity);
            entity.UpdateDate = DateTime.Now;
            // Get parent category
            entity.Parent = _categoryRepository.FindId(model.ParentSelectedValue);
            _categoryRepository.SaveChanges();
            entity.SeName = SeoHelper.ValidateSeNameAndSubmit(EntityTypeCollection.Categories, entity.Id, model.SeName, entity.Title, entity.ExternalUrl, entity.Language.Id);
            _categoryRepository.SaveChanges();
            // Create log history for editting category
            _logHistoryService.Create(new LogHistoryModel() { EntityId = entity.Id, ActionType = ActionTypeCollection.Edit });

            return new BaseResponseModel { 
                Code = HttpStatusCode.OK,
                Message = "Cập nhật danh mục thành công!"
            };
        }

        /// <summary>
        /// Delete article in db
        /// </summary>
        /// <param name="id">Category id</param>
        /// <returns></returns>
        public BaseResponseModel Delete(long id)
        {
            // Get category for checking exist and deleting
            var entity = _categoryRepository.FindId(id);
            if (entity == null)
            {
                return new BaseResponseModel
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Không tìm thấy đối tượng cần xóa!"
                };
            }
            else if (_categoryRepository.HasParent(id)) // Check category has any parent
            {
                return new BaseResponseModel
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = string.Format("{0} có nhóm con, không thể xóa", entity.Title)
                };
            }
            else if (_categoryRepository.HasArticle(id))
            {
                return new BaseResponseModel
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = string.Format("{0} có bài viết con, không thể xóa", entity.Title)
                };
            }

            // Delete Url
            SeoHelper.DeleteUrlRecord(EntityTypeCollection.Categories, entity.Id);
            // Delete category
            _categoryRepository.Delete(entity);

            // Save log History
            _logHistoryService.Insert(new LogHistoryModel() { EntityId = entity.Id, ActionType = ActionTypeCollection.Delete });

            // Delete folder
            FolderHelper.Delete(entity.ImageFolder);

            return new BaseResponseModel
            {
                Code = HttpStatusCode.OK,
                Message = string.Format("Xóa danh mục thành công!")
            };
        }

        /// <summary>
        /// Bind data to drop down list in html
        /// </summary>
        /// <param name="model"></param>
        public void BindSelectListItem(CategoryManageViewModel model)
        {
            model.DisplayTemplates = EnumHelper.ToSelectList(typeof(DisplayTemplateCollection));
            model.Languages = LanguageHelper.BindDropdown(_languageRepository.FindAll().ToList(), model.LanguageSelectedValue);
        }
    }
}
