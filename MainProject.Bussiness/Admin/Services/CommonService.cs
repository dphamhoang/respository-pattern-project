﻿using MainProject.Data;
using MainProject.Data.Repositories;
using MainProject.Bussiness.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MainProject.Bussiness.Admin.Services
{
    public class CommonService
    {
        private readonly CategoryRepository _categoryRepository;
        private readonly MenuRepository _menuRepository;

        public CommonService(MainDbContext dbContext)
        {
            _categoryRepository = new CategoryRepository(dbContext);
            _menuRepository = new MenuRepository(dbContext);
        }

        /// <summary>
        /// Get categories by language
        /// </summary>
        /// <param name="id">Language id</param>
        /// <param name="cateId">Category id</param>
        /// <param name="isFilterTemplate">Filter category by display template</param>
        /// <returns></returns>
        public List<SelectListItem> GetCategories(int id, long cateId, long execptedId)
            => CategoryHelper.BindSelectListItem(_categoryRepository, cateId, id, execptedId, true);


        public List<SelectListItem> GetMenus(int langId, int selectedValue = 0)
            => _menuRepository.FindAll().OrderBy(n => n.Order)
                                        .Select(x => new SelectListItem
                                        {
                                            Text = x.CodeName,
                                            Value = x.Id.ToString()
                                        }).ToList();
    }
}
