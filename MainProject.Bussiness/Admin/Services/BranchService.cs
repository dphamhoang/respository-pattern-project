﻿using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Data.Repositories;
using MainProject.Infrastructure.Models;
using MainProject.Bussiness.Helpers;
using MainProject.Bussiness.Admin.Models;
using MainProject.Framework.Services;
using MainProject.Framework.Models;
using System.Linq;
using System.Net;

namespace MainProject.Bussiness.Admin.Services
{
    public class BranchService
    {
        private readonly BranchRepository _branchRepository;
        private readonly LanguageRepository _languageRepository;
        private readonly LogHistoryService _logHistoryService;
        private readonly int _itemPerPage = 10;

        public BranchService(MainDbContext dbContext)
        {
            _branchRepository = new BranchRepository(dbContext);
            _languageRepository = new LanguageRepository(dbContext);
            _logHistoryService = new LogHistoryService(dbContext, EntityTypeCollection.Branch);
        }

        /// <summary>
        /// Filter branch
        /// </summary>
        /// <param name="languageId"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public IndexViewModel<MainProject.Core.Branch> GetIndex(int languageId = 0, int page = 1)
        {
            if (page < 1) page = 1;
            // Get banners by language
            var sql = _branchRepository.Find(x => x.Language.Id == languageId);

            return new IndexViewModel<MainProject.Core.Branch>()
            {
                ListItems = sql.OrderBy(d => d.Id).Skip((page - 1) * _itemPerPage).Take(_itemPerPage).ToList(),
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        Languages = LanguageHelper.BindDropdown(_languageRepository.FindAll().ToList(), languageId),
                        LanguageSelectedValue = languageId
                    },
                    BaseUrl = "/Admin/BranchAdmin/Index?"
                },
                PagingViewModel = new PagingModel(sql.Count(), _itemPerPage, page, "href='/Admin/BranchAdmin/Index?languageId=" + languageId + "&page={0}'")
            };
        }

        /// <summary>
        /// Initialize data for create view
        /// </summary>
        /// <returns></returns> 
        public BranchManageViewModel Create()
        {
            return new BranchManageViewModel
            {
                Languages = LanguageHelper.BindDropdown(_languageRepository.FindAll().ToList(), 0),
            };
        }

        /// <summary>
        /// Insert data to database
        /// </summary>
        /// <param name="model"></param>
        public BaseResponseModel Insert(BranchManageViewModel model)
        {
            var entity = new Core.Branch
            {
                Language = _languageRepository.FindId(model.LanguageSelectedValue)
            };
            // Parse data from model to entity
            model.ToEntity(ref entity, model);
            // Insert to banner Id for generating url
            _branchRepository.Insert(entity);
            // Create logHistory
            _logHistoryService.Create(new LogHistoryModel() { ActionType = ActionTypeCollection.Create, EntityId = entity.Id });

            return new BaseResponseModel
            {
                Code = HttpStatusCode.OK,
                Message = "Thêm mới chi nhánh thành công!"
            };
        }

        /// <summary>
        /// Get branch for Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BaseResponseModel<BranchManageViewModel> Edit(long id)
        {
            // Get entity bind data to model
            var entity = _branchRepository.FindUnique(x => x.Id == id);
            // Check entity is exist
            if (entity == null)
            {
                return new BaseResponseModel<BranchManageViewModel>
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Không tìm thấy đối tượng!"
                };
            }

            // Bind data to model
            var model = new BranchManageViewModel(entity);
            // Bind language select list
            BindSelectListItem(model);

            return new BaseResponseModel<BranchManageViewModel>
            {
                Code = HttpStatusCode.OK,
                Result = model
            };
        }

        /// <summary>
        /// Update data branch to databse
        /// </summary>
        /// <param name="model"></param>
        public BaseResponseModel Update(BranchManageViewModel model)
        {
            // Get entity to update data
            var entity = _branchRepository.FindUnique(d => d.Id == model.Id);
            if (entity == null)
                return new BaseResponseModel()
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Chi nhánh không tồn tại!"
                };
            // Parse data from model to entity
            model.ToEntity(ref entity, model);
			entity.Language = _languageRepository.FindId(model.LanguageSelectedValue);
            // Save changes data banner to entity
            _branchRepository.SaveChanges();
            // save logHistory 
            _logHistoryService.Create(new LogHistoryModel { EntityId = entity.Id, ActionType = ActionTypeCollection.Edit });

            return new BaseResponseModel()
            {
                Code = HttpStatusCode.OK,
                Message = "Cập nhật chi nhánh thành công!"
            };
        }

        /// <summary>
        /// Delete data banner form database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BaseResponseModel Delete(long id)
        {
            // Get entity to delete
            var entity = _branchRepository.FindUnique(x=>x.Id == id);
            // Check banner is exist
            if (entity == null)
            {
                return new BaseResponseModel()
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Không tìm thấy đối tượng cần xóa"
                };
            }
            // Delete banner
            _branchRepository.Delete(entity);
            // Save history
            _logHistoryService.Insert(new LogHistoryModel() { EntityId = entity.Id, ActionType = ActionTypeCollection.Delete });

            return new BaseResponseModel()
            {
                Code = HttpStatusCode.OK,
                Message = "Xóa thành công"
            };
        }

        /// <summary>
        /// Bind select listItem
        /// </summary>
        /// <param name="model"></param>
        public void BindSelectListItem(BranchManageViewModel model)
        {
            model.Languages = LanguageHelper.BindDropdown(_languageRepository.FindAll().ToList(), model.LanguageSelectedValue);
        }

    }
}
