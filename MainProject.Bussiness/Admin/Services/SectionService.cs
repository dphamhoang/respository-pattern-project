﻿using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Data.Repositories;
using MainProject.Infrastructure.Models;
using MainProject.Bussiness.Admin.Models;
using MainProject.Framework.Services;
using MainProject.Framework.Models;
using MainProject.Bussiness.Helpers;
using MainProject.Infrastructure.Helpers;
using System.Linq;
using System.Net;

namespace MainProject.Bussiness.Admin.Services
{
    public class SectionService
    {
        private readonly LanguageRepository _languageRepository;
        private readonly SectionRepository _sectionRepository;
        private readonly LogHistoryService _logHistoryService;

        public SectionService(MainDbContext dbContext)
        {
            _languageRepository = new LanguageRepository(dbContext);
            _sectionRepository = new SectionRepository(dbContext);
            _logHistoryService = new LogHistoryService(dbContext, EntityTypeCollection.Section);
        }

        public IndexViewModel<Section> Get(int cul)
        {
            return new IndexViewModel<Section>()
            {
                ListItems = _sectionRepository.Find(x => x.Language.Id == cul).OrderBy(x => x.Order).ToList(),
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        Languages = LanguageHelper.BindDropdown(_languageRepository.FindAll().ToList(), cul),
                        LanguageSelectedValue = cul
                    }
                }
            };
        }

        public BaseResponseModel<SectionManageModel> Edit(int id)
        {
            var entity = _sectionRepository.FindUnique(x => x.Id == id);
            if (entity == null)
            {
                return new BaseResponseModel<SectionManageModel>
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = "Không tìm thấy!"
                };
            }

            return new BaseResponseModel<SectionManageModel> { 
                Code = HttpStatusCode.OK,
                Result = new SectionManageModel(entity)
            };
        }

        public BaseResponseModel Update(SectionManageModel model)
        {
            var entity = _sectionRepository.FindUnique(x => x.Id == model.Id);
            if (entity == null) return null;

            SectionManageModel.ToEntity(model, ref entity);
            _sectionRepository.SaveChanges();

            // Save LogHistory
            _logHistoryService.Create(new LogHistoryModel() { EntityId = entity.Id, ActionType = ActionTypeCollection.Edit });

            // Release Cache
            CacheHelper.ReleaseCache("Section");

            return new BaseResponseModel()
            {
                Code = HttpStatusCode.OK,
            };
        }
    }
}
