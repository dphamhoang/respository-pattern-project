﻿using MainProject.Core;
using System;
using System.Linq;

namespace MainProject.Data.Repositories
{
    public class AlbumRepository : AbstractMainProjectRepository<Album>
    {
        private readonly MainDbContext _dbContext;
        public AlbumRepository(MainDbContext db) : base(db)
        {
            _dbContext = db;
        }

        public IQueryable<Album> Find(DateTime dateTime)
            => _dbContext.Albums.SqlQuery("Select * from Albums Where CreatedDate='" + 
                            dateTime.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'").AsQueryable();

        public void Delete(DateTime dateTime)
        {
            _dbContext.Database.ExecuteSqlCommand("Delete from Albums where CreatedDate='" + dateTime.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'");
        }
    }
}
