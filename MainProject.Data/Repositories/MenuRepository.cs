﻿using System;
using System.Collections.Generic;
using System.Linq;
using MainProject.Core;
using MainProject.Core.Enums;

namespace MainProject.Data.Repositories
{
    public class MenuRepository : AbstractMainProjectRepository<Menu>
    {
        private MainDbContext _dbContext;
        public MenuRepository(MainDbContext db)
            : base(db)
        {
            _dbContext = db;
        }

        public Menu FindId(long id)
        {
            return FindUnique(c => c.Id == id);
        }
    }
}
