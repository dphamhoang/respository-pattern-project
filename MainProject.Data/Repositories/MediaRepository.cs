﻿using MainProject.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MainProject.Data.Repositories
{
    public class MediaRepository : AbstractMainProjectRepository<Media>
    {
        private readonly MainDbContext _dbContext;

        public MediaRepository(MainDbContext db)
            : base(db)
        {
            _dbContext = db;
        }

        public IQueryable<Media> Get(DateTime dateTime)
           => _dbContext.Medias.SqlQuery(
                        "Select Medias.* from Medias Join Albums on Medias.Album_Id = Albums.Id " +
                                "Where Albums.CreatedDate = '" + dateTime.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'").AsQueryable();

        public void Inserts(List<Media> items)
        {
            items.AddRange(items);
            _dbContext.SaveChanges();
        }
    }
}
