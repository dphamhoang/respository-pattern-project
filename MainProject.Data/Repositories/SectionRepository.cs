﻿using MainProject.Core;

namespace MainProject.Data.Repositories
{
    public class SectionRepository : AbstractMainProjectRepository<Section>
    {
        public SectionRepository(MainDbContext db)
            : base(db)
        {

        }
    }
}
